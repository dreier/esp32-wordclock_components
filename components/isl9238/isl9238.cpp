#include "isl9238.h"
#include "esphome/core/log.h"

#define ISL9238_CONST_MAX_ChargeCurrent 6080       // mA
#define ISL9238_CONST_MAX_AdapterCurrent 6080      // mA
#define ISL9238_CONST_MAX_ACProchot 6400           // mA
#define ISL9238_CONST_MAX_DCProchot 12800          // mA
#define ISL9238_CONST_MAX_MaxSystemVoltage 18304   // mV
#define ISL9238_CONST_MAX_MinSystemVoltage 13824   // mV
#define ISL9238_CONST_MAX_InputVoltageLimit 18432  // mV

#define ISL9238_BIT_AutonomousChargingMode 7
#define ISL9238_BIT_BatteryLearnMode 12
#define ISL9238_BIT_TurboMode 6
#define ISL9238_BIT_BatteryShipMode 10
#define ISL9238_BIT_SMBusTimeout 7
#define ISL9238_BIT_WOCP 1
#define ISL9238_BIT_BatteryOVP 0

#define ISL9238_DEFAULT_AdapterCurrent 1024       // mA
#define ISL9238_DEFAULT_ChargeCurrent 512         // mA
#define ISL9238_DEFAULT_MinSysVoltage (3300 * 2)  // mV
#define ISL9238_DEFAULT_MaxSysVoltage (4200 * 2)  // mV
#define ISL9238_DEFAULT_ACProchot 6400            // mA
#define ISL9238_DEFAULT_DCProchot 8192            // mA
#define ISL9238_DEFAULT_InputVoltageLimit 18432   // mv

namespace esphome {
namespace isl9238 {
static const char *const TAG = "isl9238";

// https://www.farnell.com/datasheets/2712666.pdf

const uint8_t ISL9238_REGISTER_CHARGECURRENTLIMIT = 0x14;
const uint8_t ISL9238_REGISTER_MAXSYSTEMVOLTAGE = 0x15;

const uint8_t ISL9238_REGISTER_T1ANDT2 = 0x38;
const uint8_t ISL9238_REGISTER_CONTROL0 = 0x39;
const uint8_t ISL9238_REGISTER_INFORMATION1 = 0x3A;
const uint8_t ISL9238_REGISTER_ADAPTERCURRENTLIMIT2 = 0x3B;
const uint8_t ISL9238_REGISTER_CONTROL1 = 0x3C;
const uint8_t ISL9238_REGISTER_CONTROL2 = 0x3D;
const uint8_t ISL9238_REGISTER_MINSYSTEMVOLTAGE = 0x3E;
const uint8_t ISL9238_REGISTER_ADAPTERCURRENTLIMIT1 = 0x3F;

const uint8_t ISL9238_REGISTER_REVISION_ID = 0x44;

const uint8_t ISL9238_REGISTER_ACPROCHOT = 0x47;
const uint8_t ISL9238_REGISTER_DCPROCHOT = 0x48;
const uint8_t ISL9238_REGISTER_OTG_VOLTAGE = 0x49;
const uint8_t ISL9238_REGISTER_OTG_CURRENT = 0x4A;
const uint8_t ISL9238_REGISTER_VINVOLTAGE = 0x4B;
const uint8_t ISL9238_REGISTER_CONTROL3 = 0x4C;
const uint8_t ISL9238_REGISTER_INFORMATION2 = 0x4D;
const uint8_t ISL9238_REGISTER_CONTROL4 = 0x4E;

const uint8_t ISL9238_REGISTER_MANUFACTURER_ID = 0xFE;
const uint8_t ISL9238_REGISTER_DEVICE_ID = 0xFF;

/*Constants*/
const uint8_t ISL9238_CONST_DEVICE_ID = 0x0C;
const uint8_t ISL9238_CONST_MANUFACTURER_ID = 0x49;
const uint8_t ISL9238_CONST_REVISION1_ID = 0x01;
const uint8_t ISL9238_CONST_REVISION2_ID = 0x02;
const uint8_t ISL9238_CONST_REVISION3_ID = 0x06;

/* I2C Communication Functions */

void ISL9238Component::readISLRegisterBlock(uint8_t *destination, uint8_t startAddress, uint8_t bytesToRead) {
  this->read_register(startAddress, (uint8_t *) &destination, bytesToRead);
}

uint16_t ISL9238Component::readISLRegister(uint8_t registerAddress) {
  uint16_t data;
  this->read_register(registerAddress, (uint8_t *) &data, 2);
  return data;
}

void ISL9238Component::writeISLRegister(uint8_t registerAddress, uint16_t data) {
  this->write_register(registerAddress, (uint8_t *) &data, 2, true);
}

void ISL9238Component::setBitInISLRegister(uint8_t registerAddress, uint8_t bit, uint8_t val) {
  uint16_t data = readISLRegister(registerAddress);
  data = (data & (~(1 << bit))) | ((val > 0) << bit);
  writeISLRegister(registerAddress, data);
}

/*                      */

void ISL9238Component::setup() {
  ESP_LOGCONFIG(TAG, "Setting up ISL9238...");

  isl9238_reg_ManufacturerID = readISLRegister(ISL9238_REGISTER_MANUFACTURER_ID);
  isl9238_reg_RevisionID = readISLRegister(ISL9238_REGISTER_REVISION_ID);

  this->componentSetupDone = false;
  if ((this->read_register(ISL9238_REGISTER_DEVICE_ID, (uint8_t *) (&isl9238_reg_DeviceID), 2, true) != 0) ||
      (isl9238_reg_DeviceID != ISL9238_CONST_DEVICE_ID) ||
      (isl9238_reg_ManufacturerID != ISL9238_CONST_MANUFACTURER_ID) ||
      !((isl9238_reg_RevisionID == ISL9238_CONST_REVISION1_ID) ||
        (isl9238_reg_RevisionID == ISL9238_CONST_REVISION2_ID) ||
        (isl9238_reg_RevisionID == ISL9238_CONST_REVISION3_ID))) {
    this->mark_failed();
    return;
  }

  this->isl9238_reg_ChargeCurrentLimit = readISLRegister(ISL9238_REGISTER_CHARGECURRENTLIMIT);
  this->isl9238_reg_MaxSystemVoltage = readISLRegister(ISL9238_REGISTER_MAXSYSTEMVOLTAGE);
  this->isl9238_reg_T1andT2.val = readISLRegister(ISL9238_REGISTER_T1ANDT2);
  this->isl9238_reg_Control0.val = readISLRegister(ISL9238_REGISTER_CONTROL0);
  this->isl9238_reg_Information1.val = readISLRegister(ISL9238_REGISTER_INFORMATION1);
  this->isl9238_reg_AdapterCurrentLimit2 = readISLRegister(ISL9238_REGISTER_ADAPTERCURRENTLIMIT2);
  this->isl9238_reg_Control1.val = readISLRegister(ISL9238_REGISTER_CONTROL1);
  this->isl9238_reg_Control2.val = readISLRegister(ISL9238_REGISTER_CONTROL2);
  this->isl9238_reg_MinSystemVoltage = readISLRegister(ISL9238_REGISTER_MINSYSTEMVOLTAGE);
  this->isl9238_reg_AdapterCurrentLimit1 = readISLRegister(ISL9238_REGISTER_ADAPTERCURRENTLIMIT1);
  this->isl9238_reg_ACProchot = readISLRegister(ISL9238_REGISTER_ACPROCHOT);
  this->isl9238_reg_DCProchot = readISLRegister(ISL9238_REGISTER_DCPROCHOT);
  this->isl9238_reg_OTGVoltage = readISLRegister(ISL9238_REGISTER_OTG_VOLTAGE);
  this->isl9238_reg_OTGCurrent = readISLRegister(ISL9238_REGISTER_OTG_CURRENT);
  this->isl9238_reg_VINVoltage = readISLRegister(ISL9238_REGISTER_VINVOLTAGE);
  this->isl9238_reg_Control3.val = readISLRegister(ISL9238_REGISTER_CONTROL3);
  this->isl9238_reg_Information2.val = readISLRegister(ISL9238_REGISTER_INFORMATION2);
  this->isl9238_reg_Control4.val = readISLRegister(ISL9238_REGISTER_CONTROL4);

  // setze Systemspannung, Strom, Zellenzahl...
  setMinSysVoltage(ISL9238_DEFAULT_MinSysVoltage);
  setMaxSysVoltage(ISL9238_DEFAULT_MaxSysVoltage);
  setBatteryShipMode(0);    // 1=enable 0=disable
  setSMBusTimeout(1);       // 0=enable 1=disable
  setWOCPFunction(0);       // 0=enable 1=disable
  seBatteryOVPFunction(1);  // 1=enable 0=disable
  setACProchot(ISL9238_DEFAULT_ACProchot);
  setDCProchot(ISL9238_DEFAULT_DCProchot);
  setAdapterCurrentLimit1(ISL9238_DEFAULT_AdapterCurrent);
  setChargingCurrentLimit(ISL9238_DEFAULT_ChargeCurrent);
  setInputVoltageLimit(4096.0f);  // ISL9238_DEFAULT_InputVoltageLimit //The minimum input voltage for the ISL chip to
                                  // start generating VSYS or charging
  setAudioFilter(true);
  // setTurboMode(0); //0=enable 1=disable
  // setAutonomousChargingMode(0); //0=enable 1=disable
  this->componentSetupDone = true;
}

void ISL9238Component::update() {
  float temp = 42.0;

  this->isl9238_reg_Information1.val = readISLRegister(ISL9238_REGISTER_INFORMATION1);
  this->isl9238_reg_Information2.val = readISLRegister(ISL9238_REGISTER_INFORMATION2);

  for (ISL9238SensorListener *listener : this->sensorListeners) {
    listener->update();
  }
  ESP_LOGD(TAG, "ISL Info1=0x%04x", this->isl9238_reg_Information1.val);
  ESP_LOGD(TAG, "ISL Info2=0x%04x", this->isl9238_reg_Information2.val);
  // ESP_LOGD(TAG, "REV=0x%04x", getRevisionID());
  // ESP_LOGD(TAG, "Max VSYS=%f", getMaxSysVoltage());
}

void ISL9238Component::dump_config() {
  ESP_LOGCONFIG(TAG, "isl9238:");
  LOG_I2C_DEVICE(this);
  LOG_UPDATE_INTERVAL(this);
  if (this->is_failed()) {
    ESP_LOGE(TAG, "Communication with isl9238 failed!");
  }
  // LOG_PIN("  Pin CS: ", this->cs_pin_);
  // LOG_PIN("  Pin CLK: ", this->clk_pin_);
  // LOG_PIN("  Pin DIO: ", this->dio_pin_);
}

float ISL9238Component::get_setup_priority() const { return setup_priority::DATA; }

bool ISL9238Component::getBinSensorValue(std::string id) {
  bool value = false;
  if ("acok" == id) {
    value = this->isl9238_reg_Information2.reg.acok_pin_status;
  } else if ("batgone" == id) {
    value = !this->isl9238_reg_Information2.reg.batgone_pin_status;
  } else if ("gencomp" == id) {
    value = this->isl9238_reg_Information2.reg.general_purpose_comparator;
  } else if ("dcprochot" == id) {
    value = this->isl9238_reg_Information1.reg.dc_prochot_tripped;
  } else if ("acprochot" == id) {
    value = this->isl9238_reg_Information1.reg.ac_prochot_tripped;
  } else if ("intref" == id) {
    value = this->isl9238_reg_Information1.reg.internal_reference_active;
  }
  return value;
}

// float ISL9238Component::getSysVoltage() { return 0.0f; }

// float ISL9238Component::getAMON() { return 0.0f; }

// float ISL9238Component::getBMON() { return 0.0f; }

// float ISL9238Component::getPSYS() { return 0.0f; }

void ISL9238Component::setChargingCurrentLimit(float cur) {
  // if (10mR Shunt) {
  if (cur <= ISL9238_CONST_MAX_ChargeCurrent) {
    isl9238_reg_ChargeCurrentLimit = ((uint16_t) cur) & 0x1ffc;
    writeISLRegister(ISL9238_REGISTER_CHARGECURRENTLIMIT, isl9238_reg_ChargeCurrentLimit);
  }
  // } else if (5mR Shunt) {
  //   if (cur <= ISL9238_CONST_MAX_ChargeCurrent * 2) {
  //     isl9238_reg_ChargeCurrentLimit = (((uint16_t) cur) > 2) & 0x1ffc;
  //     writeISLRegister(ISL9238_REGISTER_CHARGECURRENTLIMIT, isl9238_reg_ChargeCurrentLimit);
  //   }
  // } else {
  // }
}

void ISL9238Component::setAdapterCurrentLimit1(float cur) {
  // if (20mR Shunt) {
  if (cur <= ISL9238_CONST_MAX_AdapterCurrent) {
    isl9238_reg_AdapterCurrentLimit1 = ((uint16_t) cur) & 0x1ffc;
    writeISLRegister(ISL9238_REGISTER_ADAPTERCURRENTLIMIT1, isl9238_reg_AdapterCurrentLimit1);
  }
  // } else if (10mR Shunt) {
  //   if (cur <= ISL9238_CONST_MAX_AdapterCurrent * 2) {
  //     isl9238_reg_AdapterCurrentLimit1 = (((uint16_t) cur) > 2) & 0x1ffc;
  //     writeISLRegister(ISL9238_REGISTER_ADAPTERCURRENTLIMIT1, isl9238_reg_AdapterCurrentLimit1);
  //   }
  // } else {
  // }
}

void ISL9238Component::setACProchot(float cur) {
  if (cur <= ISL9238_CONST_MAX_ACProchot) {
    isl9238_reg_ACProchot = ((uint16_t) cur) & 0x1f80;
    writeISLRegister(ISL9238_REGISTER_ACPROCHOT, isl9238_reg_ACProchot);
  } else {
  }
}

void ISL9238Component::setDCProchot(float cur) {
  if (cur <= ISL9238_CONST_MAX_DCProchot) {
    isl9238_reg_DCProchot = ((uint16_t) cur) & 0x3f00;
    writeISLRegister(ISL9238_REGISTER_DCPROCHOT, isl9238_reg_DCProchot);
  } else {
  }
}

void ISL9238Component::setMinSysVoltage(float vol) {
  if (vol <= ISL9238_CONST_MAX_MinSystemVoltage) {
    isl9238_reg_MinSystemVoltage = ((uint16_t) vol) & 0x3f00;
    writeISLRegister(ISL9238_REGISTER_MINSYSTEMVOLTAGE, isl9238_reg_MinSystemVoltage);
  } else {
  }
}

void ISL9238Component::setMaxSysVoltage(float vol) {
  if (vol <= ISL9238_CONST_MAX_MaxSystemVoltage) {
    isl9238_reg_MaxSystemVoltage = ((uint16_t) vol) & 0x7ff8;
    writeISLRegister(ISL9238_REGISTER_MAXSYSTEMVOLTAGE, isl9238_reg_MaxSystemVoltage);
  } else {
  }
}

float ISL9238Component::getMaxSysVoltage() { return (float) isl9238_reg_MaxSystemVoltage; }

void ISL9238Component::setInputVoltageLimit(float vol) {
  if (vol <= ISL9238_CONST_MAX_InputVoltageLimit) {
    isl9238_reg_VINVoltage = ((uint16_t) (vol / 341.3f)) << 8;
    writeISLRegister(ISL9238_REGISTER_VINVOLTAGE, isl9238_reg_VINVoltage);
  } else {
  }
}

float ISL9238Component::getInputVoltageLimit() {
  float vol = ((float) (readISLRegister(ISL9238_REGISTER_VINVOLTAGE) >> 8)) * 341.3f;
  return vol;
}

void ISL9238Component::setAutonomousChargingMode(bool en) {
  setBitInISLRegister(ISL9238_REGISTER_CONTROL3, ISL9238_BIT_AutonomousChargingMode, en);
}

void ISL9238Component::setBatteryLearnMode(bool en) {
  setBitInISLRegister(ISL9238_REGISTER_CONTROL1, ISL9238_BIT_BatteryLearnMode, en);
}

void ISL9238Component::setAudioFilter(bool en) {
  if (this->isl9238_reg_Control1.reg.AudioFilter != en) {
    this->isl9238_reg_Control1.reg.AudioFilter = en;
    writeISLRegister(ISL9238_REGISTER_CONTROL1, this->isl9238_reg_Control1.val);
  }
}

void ISL9238Component::setTurboMode(bool en) {
  setBitInISLRegister(ISL9238_REGISTER_CONTROL1, ISL9238_BIT_TurboMode, en);
}

void ISL9238Component::setBatteryShipMode(bool en) {
  setBitInISLRegister(ISL9238_REGISTER_CONTROL3, ISL9238_BIT_BatteryShipMode, en);
}

void ISL9238Component::setSMBusTimeout(bool en) {
  setBitInISLRegister(ISL9238_REGISTER_CONTROL0, ISL9238_BIT_SMBusTimeout, en);
}

void ISL9238Component::setWOCPFunction(bool en) {
  setBitInISLRegister(ISL9238_REGISTER_CONTROL2, ISL9238_BIT_WOCP, en);
}

void ISL9238Component::seBatteryOVPFunction(bool en) {
  setBitInISLRegister(ISL9238_REGISTER_CONTROL2, ISL9238_BIT_BatteryOVP, en);
}

uint8_t ISL9238Component::getDeviceID() {
  if (!componentSetupDone) {
    this->isl9238_reg_DeviceID = readISLRegister(ISL9238_REGISTER_DEVICE_ID);
  }
  return (uint8_t) this->isl9238_reg_DeviceID;
}

uint8_t ISL9238Component::getManufacturerID() {
  if (!componentSetupDone) {
    this->isl9238_reg_ManufacturerID = readISLRegister(ISL9238_REGISTER_MANUFACTURER_ID);
  }
  return (uint8_t) this->isl9238_reg_ManufacturerID;
}

uint8_t ISL9238Component::getRevisionID() {
  if (!componentSetupDone) {
    this->isl9238_reg_RevisionID = readISLRegister(ISL9238_REGISTER_REVISION_ID);
  }
  return (uint8_t) this->isl9238_reg_RevisionID;
}

}  // namespace isl9238
}  // namespace esphome