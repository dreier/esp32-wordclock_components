#pragma once

// https://www.farnell.com/datasheets/2712666.pdf

#include "esphome/core/component.h"
#include "esphome/core/hal.h"
#include "esphome/components/i2c/i2c.h"
#include "esphome/core/helpers.h"
#include <map>
#include <string>
// #include "esphome/components/sensor/sensor.h"
// #include "esphome/components/binary_sensor/binary_sensor.h"
// #include "esphome/components/gpio/binary_sensor/gpio_binary_sensor.h"
namespace esphome {
namespace isl9238 {

/* Typedefs */

typedef enum {
  isl9238_control_loop_mode_max_system_voltage = 0b00,
  isl9238_control_loop_mode_charging_current = 0b01,
  isl9238_control_loop_mode_adapter_current_limit = 0b10,
  isl9238_control_loop_mode_input_voltage = 0b11,
} isl9238_control_loop_mode_t;

typedef enum {
  isl9238_operation_mode_unknown1 = 0b000,  // not described in datasheet
  isl9238_operation_mode_boost = 0b001,
  isl9238_operation_mode_buck = 0b010,
  isl9238_operation_mode_buck_boost = 0b011,
  isl9238_operation_mode_unknown2 = 0b100,  // not described in datasheet
  isl9238_operation_mode_otg_boost_bode = 0b101,
  isl9238_operation_mode_otg_buck_bode = 0b110,
  isl9238_operation_mode_otg_buck_boost = 0b111
} isl9238_operation_mode_t;

typedef enum {
  sl9238_state_machine_status_off = 0b0000,
  sl9238_state_machine_status_battery = 0b0001,
  sl9238_state_machine_status_adapter = 0b0010,
  sl9238_state_machine_status_acok = 0b0011,
  sl9238_state_machine_status_vsys = 0b0100,
  sl9238_state_machine_status_charge = 0b0101,
  sl9238_state_machine_status_enotg = 0b0110,
  sl9238_state_machine_status_otg = 0b0111,
  sl9238_state_machine_status_enld05 = 0b1000,
  sl9238_state_machine_status_not_applicable = 0b1001,
  sl9238_state_machine_status_trim_enchref = 0b1010,
  sl9238_state_machine_status_achrg = 0b1011,
  sl9238_state_machine_status_cal = 0b1100,
  sl9238_state_machine_status_agon_agontg = 0b1101,
  sl9238_state_machine_status_wait_psys = 0b1110,
  sl9238_state_machine_status_adppsys = 0b1111
} isl9238_state_machine_status_t;

typedef enum {
  isl9238_T1_duration_10ms = 0b000,
  isl9238_T1_duration_20ms = 0b001,
  isl9238_T1_duration_15ms = 0b010,
  isl9238_T1_duration_5ms = 0b011,
  isl9238_T1_duration_1ms = 0b100,
  isl9238_T1_duration_05ms = 0b101,
  isl9238_T1_duration_01ms = 0b110,
  isl9238_T1_duration_0ms = 0b111
} isl9238_T1_duration_t;

typedef enum {
  isl9238_T2_duration_10us = 0b000,
  isl9238_T2_duration_100us = 0b001,
  isl9238_T2_duration_500us = 0b010,
  isl9238_T2_duration_1ms = 0b011,
  isl9238_T2_duration_300us = 0b100,
  isl9238_T2_duration_750us = 0b101,
  isl9238_T2_duration_2ms = 0b110,
  isl9238_T2_duration_10ms = 0b111
} isl9238_T2_duration_t;

typedef union {
  struct {
    isl9238_T1_duration_t T1 : 3;
    uint16_t unused1 : 5;
    isl9238_T2_duration_t T2 : 3;
    uint16_t unused2 : 5;
  } __attribute__((packed)) reg;
  uint16_t val;
} isl9238_t1t2_t;
;

typedef union {
  struct {
    uint16_t unused1 : 4;
    bool trickle_charging : 1;
    uint16_t unused2 : 5;
    bool low_vsys_prochot_tripped : 1;
    bool dc_prochot_tripped : 1;
    bool ac_prochot_tripped : 1;
    isl9238_control_loop_mode_t control_loop_mode : 2;
    bool internal_reference_active : 1;
  } __attribute__((packed)) reg;
  uint16_t val;
} isl9238_information1_t;

typedef union {
  struct {
    uint8_t program_register : 5;
    isl9238_operation_mode_t operation_mode : 3;
    isl9238_state_machine_status_t state_machine_status : 4;
    bool batgone_pin_status : 1;
    bool general_purpose_comparator : 1;
    bool acok_pin_status : 1;
    uint16_t unused : 1;
  } __attribute__((packed)) reg;
  uint16_t val;
} isl9238_information2_t;

typedef enum {
  isl9238_PhaseCompOffset_rev1_p0mV = 0b000,
  isl9238_PhaseCompOffset_rev1_p0m5V = 0b001,
  isl9238_PhaseCompOffset_rev1_p1mV = 0b010,
  isl9238_PhaseCompOffset_rev1_p1m5v = 0b011,
  isl9238_PhaseCompOffset_rev1_n2mV = 0b100,
  isl9238_PhaseCompOffset_rev1_n1m5V = 0b101,
  isl9238_PhaseCompOffset_rev1_n1mV = 0b110,
  isl9238_PhaseCompOffset_rev1_n0m5V = 0b111
} isl9238_PhaseCompOffset_rev1_t;

typedef enum {
  isl9238_PhaseCompOffset_rev2_p0mV = 0b000,
  isl9238_PhaseCompOffset_rev2_p1mV = 0b001,
  isl9238_PhaseCompOffset_rev2_p2mV = 0b010,
  isl9238_PhaseCompOffset_rev2_p3mV = 0b011,
  isl9238_PhaseCompOffset_rev2_n4mV = 0b100,
  isl9238_PhaseCompOffset_rev2_n3mV = 0b101,
  isl9238_PhaseCompOffset_rev2_n2mV = 0b110,
  isl9238_PhaseCompOffset_rev2_n1mV = 0b111
} isl9238_PhaseCompOffset_rev2_t;

typedef union {
  isl9238_PhaseCompOffset_rev1_t rev1;
  isl9238_PhaseCompOffset_rev2_t rev2;
  uint16_t val;
} isl9238_PhaseCompOffset_t;

typedef union {
  isl9238_PhaseCompOffset_rev1_t R5m;
  isl9238_PhaseCompOffset_rev2_t R10m;
  isl9238_PhaseCompOffset_rev2_t R20m;
  uint16_t val;
} isl9238_DCProchotThresholdInBatteryOnlyLowPowerMode_t;

typedef enum {
  isl9238_HighSideFETShortDetectionThreshold_400mV = 0b00,
  isl9238_HighSideFETShortDetectionThreshold_500mV = 0b01,
  isl9238_HighSideFETShortDetectionThreshold_600mV = 0b10,
  isl9238_HighSideFETShortDetectionThreshold_800mV = 0b11
} isl9238_HighSideFETShortDetThres_t;

typedef union {
  struct {
    uint16_t ReverseBuckandBuckBoostPhaseComparatorThresholdOffset_0 : 1;
    uint16_t unused : 1;
    bool InputVoltageRegulationLoop : 1;
    uint16_t DCProchotThresholdInBatteryOnlyLowPowerMode : 2;
    uint16_t HighSideFETShortDetectionThreshold : 2;
    bool SMBusTimeout : 1;
    uint16_t ReverseBuckAndBuckBoostPhaseComparatorThresholdOffset_98 : 2;
    isl9238_PhaseCompOffset_rev1_t ForwardAndReverseBoostPhaseComparatorThresholdOffset : 3;
    union {
      isl9238_PhaseCompOffset_rev1_t rev1 : 3;
      isl9238_PhaseCompOffset_rev2_t rev2 : 3;
      uint16_t val : 3;
    } ForwardBuckAndBuckBoostPhaseComparatorThresholdOffset;
  } __attribute__((packed)) reg;
  uint16_t val;
} isl9238_control0_t;

typedef enum {
  isl9238_control1_ProchotReference_6V0 = 0b00,  // default
  isl9238_control1_ProchotReference_6V3 = 0b01,
  isl9238_control1_ProchotReference_6V6 = 0b10,
  isl9238_control1_ProchotReference_6V9 = 0b11
} isl9238_control1_ProchotReference_t;

typedef enum {
  isl9238_control1_SwitchingFrequency_PROG = 0b00,  // default
  isl9238_control1_SwitchingFrequency_839kHz = 0b01,
  isl9238_control1_SwitchingFrequency_723kHz = 0b10,
  isl9238_control1_SwitchingFrequency_635kHz = 0b11
} isl9238_control1_SwitchingFrequency_t;

typedef enum {
  isl9238_control1_ComperatorDebounceTime_2us = 0b00,  // default
  isl9238_control1_ComperatorDebounceTime_12us = 0b01,
  isl9238_control1_ComperatorDebounceTime_5ms = 0b10,
  isl9238_control1_ComperatorDebounceTime_5s = 0b11
} isl9238_control1_ComperatorDebounceTime_t;

typedef union {
  struct {
    isl9238_control1_ProchotReference_t Low_VSYS_ProchotReference : 2;
    bool vsys : 1;
    bool psys : 1;
    bool ABMonSelect : 1;
    bool ABMonFunction : 1;
    bool Turbo : 1;
    uint16_t unused : 1;
    isl9238_control1_SwitchingFrequency_t SwitchingFrequency : 2;
    bool AudioFilter : 2;
    bool OTGFunction : 1;
    bool LearnMode : 1;
    bool ExitLearnModeOption : 1;
    isl9238_control1_ComperatorDebounceTime_t generalPurposeComperatorAssertionDebounceTime : 2;
  } __attribute__((packed)) reg;
  uint16_t val;
} isl9238_control1_t;

typedef enum {
  isl9238_control2_CMINReference_1V2 = 0b0,  // default
  isl9238_control2_CMINReference_2V = 0b1
} isl9238_control2_CMINReference_t;

typedef enum {
  isl9238_control2_PROCHOTDuration_0ms = 0b000,  // default
  isl9238_control2_PROCHOTDuration_20ms = 0b001,
  isl9238_control2_PROCHOTDuration_15ms = 0b010,
  isl9238_control2_PROCHOTDuration_5ms = 0b011,
  isl9238_control2_PROCHOTDuration_1ms = 0b100,
  isl9238_control2_PROCHOTDuration_500us = 0b101,
  isl9238_control2_PROCHOTDuration_100us = 0b110,
  isl9238_control2_PROCHOTDuration_0s = 0b111
} isl9238_control2_PROCHOTDuration_t;

typedef enum {
  isl9238_control2_PROCHOTDebounce_7us = 0b00,  // default
  isl9238_control2_PROCHOTDebounce_100us = 0b01,
  isl9238_control2_PROCHOTDebounce_500us = 0b10,
  isl9238_control2_PROCHOTDebounce_1ms = 0b11
} isl9238_control2_PROCHOTDebounce_t;

typedef enum {
  isl9238_control2_AdapterInsertionToSwitchingDebounce_1s3 = 0b0,
  isl9238_control2_AdapterInsertionToSwitchingDebounce_150ms = 0b1
} isl9238_control2_AdapterInsertionToSwitchingDebounce_t;

typedef enum {
  isl9238_control2_OTGFunctionEnableDebounceTime_1s3 = 0b0,
  isl9238_control2_OTGFunctionEnableDebounceTime_150ms = 0b1
} isl9238_control2_OTGFunctionEnableDebounceTime_t;

typedef enum {
  isl9238_control2_TrickleChargingCurrent_256mA = 0b00,
  isl9238_control2_TrickleChargingCurrent_128mA = 0b01,
  isl9238_control2_TrickleChargingCurrent_64mA = 0b10,
  isl9238_control2_TrickleChargingCurrent_512mA = 0b11
} isl9238_control2_TrickleChargingCurrent_t;

typedef union {
  struct {
    bool BatteryOVPFunction : 1;
    bool WOCPFunction : 1;
    bool CMOUTPolarity : 1;
    bool GeneralPurposeComparator : 1;
    isl9238_control2_CMINReference_t CMINReference : 1;
    bool ASGATEinOTGMode : 1;
    isl9238_control2_PROCHOTDuration_t PROCHOTDuration : 3;
    isl9238_control2_PROCHOTDebounce_t PROCHOTDebounce : 2;
    isl9238_control2_AdapterInsertionToSwitchingDebounce_t AdapterInsertionToSwitchingDebounce : 1;
    bool TwoLevelAdapterCurrentLimitFunction : 1;
    isl9238_control2_OTGFunctionEnableDebounceTime_t OTGFunctionEnableDebounceTime : 1;
    isl9238_control2_TrickleChargingCurrent_t TrickleChargingCurrent : 2;
  } __attribute__((packed)) reg;
  uint16_t val;
} isl9238_control2_t;

typedef enum {
  isl9238_control3_BuckBoostSwitchingPeriod_x1 = 0b0,
  isl9238_control3_BuckBoostSwitchingPeriod_x2 = 0b1
} isl9238_control3_BuckBoostSwitchingPeriod_t;

typedef enum {
  isl9238_control3_ABMONDirection_AdapterInCurrent = 0b0,
  isl9238_control3_ABMONDirection_OTGOutCurrent = 0b1
} isl9238_control3_ABMONDirection_t;

typedef enum {
  isl9238_control3_ACAndCCFeedbackGain_x1 = 0b0,
  isl9238_control3_ACAndCCFeedbackGain_0x5 = 0b1
} isl9238_control3_ACAndCCFeedbackGain_t;

typedef enum {
  isl9238_control3_ExitIDMTimer_40ms = 0b0,
  isl9238_control3_ExitIDMTimer_80ms = 0b1
} isl9238_control3_ExitIDMTimer_t;

typedef enum {
  isl9238_control3_PSYSGain_1uA44 = 0b0,  // default
  isl9238_control3_PSYSGain_0uA723 = 0b1
} isl9238_control3_PSYSGain_t;

typedef enum {
  isl9238_control3_ChargerTimeout_175s = 0b00,
  isl9238_control3_ChargerTimeout_87s5 = 0b01,
  isl9238_control3_ChargerTimeout_43s75 = 0b10,
  isl9238_control3_ChargerTimeout_5s = 0b11
} isl9238_control3_ChargerTimeout_t;

typedef enum {
  isl9238_control3_AutonomousChargingTerminationTime_20ms = 0b0,
  isl9238_control3_AutonomousChargingTerminationTime_200ms = 0b1
} isl9238_control3_AutonomousChargingTerminationTime_t;

typedef union {
  struct {
    bool OTGStartUpDelay : 1;
    isl9238_control3_BuckBoostSwitchingPeriod_t BuckBoostSwitchingPeriod : 1;
    bool DigitalReset : 1;
    isl9238_control3_ABMONDirection_t ABMONDirection : 1;
    bool InputCurrentLimitLoopWhenBATGONE : 1;
    bool InputCurrentLimitLoop : 1;
    isl9238_control3_ACAndCCFeedbackGain_t ACAndCCFeedbackGain : 1;
    bool AutonomousChargingMode : 1;
    isl9238_control3_ExitIDMTimer_t ExitIDMTimer : 1;
    isl9238_control3_PSYSGain_t PSYSGain : 1;
    bool BGATEOFF : 1;
    isl9238_control3_ChargerTimeout_t ChargerTimeout : 2;
    isl9238_control3_AutonomousChargingTerminationTime_t AutonomousChargingTerminationTime : 1;
    bool ReloadACLIMWhenAdapterIsPluggedIn : 1;
    bool RereadPROGPinResistor : 1;
  } __attribute__((packed)) reg;
  uint16_t val;
} isl9238_control3_t;

typedef enum {
  isl9238_control4_ACOKFallingOrBATGONERisingDebounce_2us = 0b00,
  isl9238_control4_ACOKFallingOrBATGONERisingDebounce_25us = 0b01,
  isl9238_control4_ACOKFallingOrBATGONERisingDebounce_125us = 0b00,
  isl9238_control4_ACOKFallingOrBATGONERisingDebounce_250us = 0b01
} isl9238_control4_ACOKFallingOrBATGONERisingDebounce_t;

typedef union {
  struct {
    uint16_t unused : 8;
    bool OTGCURRENTPROCHOT : 1;
    bool BATGONEPROCHOT : 1;
    bool ACOKPROCHOT : 1;
    bool ComparatorPROCHOT : 1;
    isl9238_control4_ACOKFallingOrBATGONERisingDebounce_t ACOKFallingOrBATGONERisingDebounce : 2;
    bool PROCHOTClear : 1;
    bool PROCHOTLatch : 1;
  } __attribute__((packed)) reg;
  uint16_t val;
} isl9238_control4_t;

///////////////////////////////////////////////////////////////////

class ISL9238SensorListener {
 private:
  // virtual ISL9238Component *islComponent;

 public:
  virtual void update(){};

 protected:
};

class ISL9238Component : public PollingComponent, public i2c::I2CDevice {
 private:
  std::vector<ISL9238SensorListener *> sensorListeners{};
  bool componentSetupDone = false;

  void setBitInISLRegister(uint8_t registerAddress, uint8_t bit, uint8_t val);
  uint16_t readISLRegister(uint8_t registerAddress);
  void writeISLRegister(uint8_t registerAddress, uint16_t data);
  void readISLRegisterBlock(uint8_t *destination, uint8_t startAddress, uint8_t bytesToRead);

 public:
  // ISL9238Component();
  //~ISL9238Component();

  void setup() override;
  void dump_config() override;
  void update() override;
  float get_setup_priority() const override;

  void register_sensors(ISL9238SensorListener *sense) { this->sensorListeners.push_back(sense); }
  bool getBinSensorValue(std::string id);

  ///////////////

  void setChargingCurrentLimit(float cur);
  float getChargingCurrentLimit();
  void setMaxSysVoltage(float vol);
  float getMaxSysVoltage();
  void setTwoLevelAdapterCurrentLimitT1(isl9238_T1_duration_t t);
  float getTwoLevelAdapterCurrentLimitT1();
  void setTwoLevelAdapterCurrentLimitT2(isl9238_T2_duration_t t);
  float getTwoLevelAdapterCurrentLimitT2();

  /* start Control0*/
  void setInputVoltageRegulationLoop(bool en);
  void setDCProchotThresholdInBatteryOnlyLowPowerMode(isl9238_DCProchotThresholdInBatteryOnlyLowPowerMode_t threshold);
  void setHighSideFETShortDetectionThreshold(isl9238_HighSideFETShortDetThres_t threshold);
  void setSMBusTimeout(bool en);
  void setReverseBuckAndBuckBoostPhaseComparatorThresholdOffset(isl9238_PhaseCompOffset_t offset);
  void setForwardAndReverseBoostPhaseComparatorThresholdOffset(isl9238_PhaseCompOffset_rev1_t offset);
  void setForwardBuckandBuckBoostPhaseComparatorThresholdOffset(isl9238_PhaseCompOffset_t offset);
  /* end Control0*/

  /* start Information1*/
  bool getState_trickle_charging();
  bool getState_low_vsys_prochot_tripped();
  bool getState_dc_prochot_tripped();
  bool getState_ac_prochot_tripped();
  isl9238_control_loop_mode_t getConfig_control_loop_mode();
  bool getState_internal_reference_active();
  /* end Information1*/

  void setAdapterCurrentLimit2(float cur);

  /* start Control1*/
  void setLow_VSYS_ProchotReference(isl9238_control1_ProchotReference_t ref);
  void setVsys(bool en);
  void setPsys(bool en);
  void setABMonSelect(bool en);
  void setABMonFunction(bool en);
  void setTurboMode(bool en);
  void setSwitchingFrequency(isl9238_control1_SwitchingFrequency_t freq);
  void setAudioFilter(bool en);
  void seOTGFunction(bool en);
  void setBatteryLearnMode(bool en);
  void setExitLearnModeOption(bool en);
  void setgeneralPurposeComperatorAssertionDebounceTime(isl9238_control1_ComperatorDebounceTime_t time);
  /* end Control1*/

  /* start Control2*/
  // ToDo Missing
    void seBatteryOVPFunction(bool en);
    void setWOCPFunction(bool en);
    void setCMOUTPolarity(bool CMOUTPolarity);
    void setGeneralPurposeComparator(bool GeneralPurposeComparator);
    void setCMINReference(isl9238_control2_CMINReference_t CMINReference);
    void setASGATEinOTGMode(bool ASGATEinOTGMode);
    void setPROCHOTDuration(isl9238_control2_PROCHOTDuration_t PROCHOTDuration);
    void setPROCHOTDebounce(isl9238_control2_PROCHOTDebounce_t PROCHOTDebounce);
    void setAdapterInsertionToSwitchingDebounce(isl9238_control2_AdapterInsertionToSwitchingDebounce_t AdapterInsertionToSwitchingDebounce);
    void setTwoLevelAdapterCurrentLimitFunction(bool TwoLevelAdapterCurrentLimitFunction);
    void setOTGFunctionEnableDebounceTime(isl9238_control2_OTGFunctionEnableDebounceTime_t OTGFunctionEnableDebounceTime);
    void setTrickleChargingCurrent(isl9238_control2_TrickleChargingCurrent_t TrickleChargingCurrent);

  /* end Control2*/

  void setMinSysVoltage(float vol);
  void setAdapterCurrentLimit1(float cur);

  uint8_t getRevisionID();
  void setACProchot(float cur);
  void setDCProchot(float cur);
  void setOTGVoltage(float vol);
  void setOTGCurrent(float cur);
  void setInputVoltageLimit(float vol);
  float getInputVoltageLimit();

  /* start Control3*/
  void setAutonomousChargingMode(bool en);
  void setOTGStartUpDelay(bool OTGStartUpDelay);
  void setBuckBoostSwitchingPeriod(isl9238_control3_BuckBoostSwitchingPeriod_t BuckBoostSwitchingPeriod);
  void setDigitalReset(bool DigitalReset);
  void setABMONDirection(isl9238_control3_ABMONDirection_t ABMONDirection);
  void setInputCurrentLimitLoopWhenBATGONE(bool InputCurrentLimitLoopWhenBATGONE);
  void setInputCurrentLimitLoop(bool InputCurrentLimitLoop);
  void setACAndCCFeedbackGain(isl9238_control3_ACAndCCFeedbackGain_t ACAndCCFeedbackGain);
  void setExitIDMTimer(isl9238_control3_ExitIDMTimer_t ExitIDMTimer);
  void setPSYSGain(isl9238_control3_PSYSGain_t PSYSGain);
  void setBatteryShipMode(bool en);
  void setChargerTimeout(isl9238_control3_ChargerTimeout_t ChargerTimeout);
  void setAutonomousChargingTerminationTime( isl9238_control3_AutonomousChargingTerminationTime_t AutonomousChargingTerminationTime);
  void setReloadACLIMWhenAdapterIsPluggedIn(bool ReloadACLIMWhenAdapterIsPluggedIn);
  void setRereadPROGPinResistor(bool RereadPROGPinResistor);
  /* end Control3*/

  /* start Information2*/
  uint8_t getProgramRegister();  // Figure out what this actually is, datasheet is missing information here
  isl9238_operation_mode_t getConfig_operation_mode();
  isl9238_state_machine_status_t getConfig_state_machine_status();
  bool getState_batgone_pin_status();
  bool getState_general_purpose_comparator();
  bool getState_acok_pin_status();
  /* end Information2*/

  /* start Control4*/
  void setOTGCURRENTPROCHOT(bool OTGCURRENTPROCHOT);
  void setBATGONEPROCHOT(bool BATGONEPROCHOT);
  void setACOKPROCHOT(bool ACOKPROCHOT);
  void setComparatorPROCHOT(bool ComparatorPROCHOT);
  void setACOKFallingOrBATGONERisingDebounce(
      isl9238_control4_ACOKFallingOrBATGONERisingDebounce_t ACOKFallingOrBATGONERisingDebounce);
  void setPROCHOTClear(bool PROCHOTClear);
  void setPROCHOTLatch(bool PROCHOTLatch);
  /* end Control4*/

  uint8_t getManufacturerID();
  uint8_t getDeviceID();

 protected:
  uint16_t isl9238_reg_ChargeCurrentLimit;
  uint16_t isl9238_reg_MaxSystemVoltage;
  isl9238_t1t2_t isl9238_reg_T1andT2;
  isl9238_control0_t isl9238_reg_Control0;
  isl9238_information1_t isl9238_reg_Information1;
  uint16_t isl9238_reg_AdapterCurrentLimit2;
  isl9238_control1_t isl9238_reg_Control1;
  isl9238_control2_t isl9238_reg_Control2;
  uint16_t isl9238_reg_MinSystemVoltage;
  uint16_t isl9238_reg_AdapterCurrentLimit1;
  uint16_t isl9238_reg_RevisionID;
  uint16_t isl9238_reg_ACProchot;
  uint16_t isl9238_reg_DCProchot;
  uint16_t isl9238_reg_OTGVoltage;
  uint16_t isl9238_reg_OTGCurrent;
  uint16_t isl9238_reg_VINVoltage;
  isl9238_control3_t isl9238_reg_Control3;
  isl9238_information2_t isl9238_reg_Information2;
  isl9238_control4_t isl9238_reg_Control4;
  uint16_t isl9238_reg_ManufacturerID;
  uint16_t isl9238_reg_DeviceID;
};
}  // namespace isl9238
}  // namespace esphome