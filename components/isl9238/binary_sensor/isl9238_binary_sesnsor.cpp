#include "isl9238_binary_sesnsor.h"
#include "esphome/core/helpers.h"
#include "esphome/core/log.h"

namespace esphome {
namespace isl9238 {
static const char *const TAG = "isl9238.binary_sensor";
static bool firstTimePublishStates;

void ISL9238BinarySensor::setup() {
  ESP_LOGCONFIG(TAG, "Setting up isl9238.binary_sensors...");
  if (islComponent->is_failed()) {
    this->mark_failed();
    return;
  }
  for (std::map<std::string, sensorPointers_t>::iterator elm = binarySensors.begin(); elm != binarySensors.end();
       ++elm) {
    sensorPointers_t binSensorStruct = elm->second;
    binary_sensor::BinarySensor *sensor = binSensorStruct.sensor;
    InternalGPIOPin *gpio = binSensorStruct.pin;
    if (gpio != nullptr) {  // gpio is not a nullpointer -> GPIO Sensor
      gpio->setup();
      LOG_PIN("  Pin: ", gpio);
    }
  }
  firstTimePublishStates = true;
}

void ISL9238BinarySensor::update() {
  for (std::map<std::string, sensorPointers_t>::iterator elm = binarySensors.begin(); elm != binarySensors.end();
       ++elm) {
    sensorPointers_t binSensorStruct = elm->second;
    bool state;

    if (binSensorStruct.pin != nullptr) {
      state = binSensorStruct.pin->digital_read();
    } else if (this->islComponent != nullptr) {
      if (binSensorStruct.sensor != nullptr) {
        state = islComponent->getBinSensorValue(elm->first);
      } else {
        continue;
      }
    } else {
      continue;
    }
    if ((state != binSensorStruct.sensor->state) || firstTimePublishStates) {
      binSensorStruct.sensor->publish_state(state);
    }
  }
  firstTimePublishStates = false;
}

void ISL9238BinarySensor::dump_config() {
  ESP_LOGCONFIG(TAG, "isl9238 Binary Sensors:");
  if (this->is_failed()) {
    ESP_LOGE(TAG, "Setting up binary sensors failed!");
  }
  for (std::map<std::string, sensorPointers_t>::iterator elm = binarySensors.begin(); elm != binarySensors.end();
       ++elm) {
    sensorPointers_t binSensorStruct = elm->second;
    LOG_BINARY_SENSOR("  ", " ", binSensorStruct.sensor);
  }
}

void ISL9238BinarySensor::register_binaryExt_sensor(std::string id, binary_sensor::BinarySensor *sensor) {
  sensorPointers_t sensorStruct = {.sensor = sensor, .pin = nullptr};
  binarySensors.insert(std::make_pair(id, sensorStruct));
}

void ISL9238BinarySensor::register_binaryGPIO_sensor(std::string id, binary_sensor::BinarySensor *sensor,
                                                     InternalGPIOPin *pin) {
  sensorPointers_t sensorStruct = {.sensor = sensor, .pin = pin};
  LOG_PIN("  Pin: ", pin);
  binarySensors.insert(std::make_pair(id, sensorStruct));
}

// float ISL9238BinarySensor::get_setup_priority() const { return setup_priority::DATA; }

}  // namespace isl9238
}  // namespace esphome