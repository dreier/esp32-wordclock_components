import esphome.codegen as cg
import esphome.config_validation as cv
from esphome import pins
from esphome.components import (
   binary_sensor
)
from .. import isl9238_ns, ISL9238Component, CONF_ISL9238_ID
from esphome.const import (
    CONF_ID,
    CONF_PIN,
    DEVICE_CLASS_BATTERY
)

CONF_SENS_ACOK="acok"
CONF_SENS_BATGONE="batgone"
CONF_SENS_OTGPG="otgpg"
CONF_SENS_PROCHOT="prochot"

CONF_SENS_PROCHOT="prochot"
CONF_SENS_DCPROCHOT="dcprochot"
CONF_SENS_ACPROCHOT="acprochot"
CONF_SENS_INTREF="intref"
CONF_SENS_GENCOMP="gencomp"

CODEOWNERS = ["@Sven3er"]

#DEPENDENCIES = ["isl9238"] #adc?

isl_binary_sensor_optional_pin = binary_sensor.binary_sensor_schema(
    #icon=ICON_BRIEFCASE_DOWNLOAD,
).extend(
    {
        cv.Optional(CONF_PIN): cv.All(pins.internal_gpio_input_pin_schema)
    }
)

isl_binary_sensor_required_pin = binary_sensor.binary_sensor_schema(
    #icon=ICON_BRIEFCASE_DOWNLOAD,
).extend(
    {
        cv.Required(CONF_PIN): cv.All(pins.internal_gpio_input_pin_schema)
    }
)

#isl9238BinarySensor_ns = cg.esphome_ns.namespace("isl9238")
isl9238BinarySensor = isl9238_ns.class_(
    "ISL9238BinarySensor", binary_sensor.BinarySensor, cg.Component
)

CONFIG_SCHEMA = cv.All(
    cv.COMPONENT_SCHEMA.extend(
        {
            cv.GenerateID(): cv.declare_id(isl9238BinarySensor),
            cv.GenerateID(CONF_ISL9238_ID): cv.use_id(ISL9238Component),
            cv.Optional(CONF_SENS_BATGONE): isl_binary_sensor_optional_pin,
            cv.Optional(CONF_SENS_ACOK): isl_binary_sensor_optional_pin,

            cv.Optional(CONF_SENS_PROCHOT): isl_binary_sensor_required_pin,
            cv.Optional(CONF_SENS_OTGPG): isl_binary_sensor_required_pin,

            cv.Optional(CONF_SENS_DCPROCHOT): binary_sensor.binary_sensor_schema(),
            cv.Optional(CONF_SENS_ACPROCHOT): binary_sensor.binary_sensor_schema(),
            cv.Optional(CONF_SENS_INTREF): binary_sensor.binary_sensor_schema(),
            cv.Optional(CONF_SENS_GENCOMP): binary_sensor.binary_sensor_schema(),
        }
    ),
)

async def to_code(config):
    null = 0
    var = cg.new_Pvariable(config[CONF_ID])
    await cg.register_component(var, config)
    isl9238 = await cg.get_variable(config[CONF_ISL9238_ID])

    if batgone_config := config.get(CONF_SENS_BATGONE):
        batgone = await binary_sensor.new_binary_sensor(batgone_config)
        cg.add(var.register_isl_component(isl9238))
        if CONF_PIN in batgone_config:
            #if GPIO used treat as GPIO sensor
            gpio = await cg.gpio_pin_expression(batgone_config[CONF_PIN])
            cg.add(var.register_binaryGPIO_sensor(CONF_SENS_BATGONE,batgone,gpio))
        else:
            #if no GPIO used read status via I2C         
            cg.add(var.register_binaryExt_sensor(CONF_SENS_BATGONE,batgone))

    if acok_config := config.get(CONF_SENS_ACOK):
        acok = await binary_sensor.new_binary_sensor(acok_config)
        cg.add(var.register_isl_component(isl9238))
        if CONF_PIN in acok_config:
            #if GPIO used treat as GPIO sensor
            gpio = await cg.gpio_pin_expression(acok_config[CONF_PIN])
            cg.add(var.register_binaryGPIO_sensor(CONF_SENS_ACOK, acok, gpio))
        else:
            #if no GPIO used read status via I2C         
            cg.add(var.register_binaryExt_sensor(CONF_SENS_ACOK, acok))
        

    if prochot_config := config.get(CONF_SENS_PROCHOT):
        prochot = await binary_sensor.new_binary_sensor(prochot_config)
        #if GPIO used treat as GPIO sensor
        gpio = await cg.gpio_pin_expression(prochot_config[CONF_PIN])
        cg.add(var.register_isl_component(isl9238))
        cg.add(var.register_binaryGPIO_sensor(CONF_SENS_PROCHOT, prochot, gpio))
        

    if otgpg_config := config.get(CONF_SENS_OTGPG):
        otgpg = await binary_sensor.new_binary_sensor(otgpg_config)
        #if GPIO used treat as GPIO sensor
        gpio = await cg.gpio_pin_expression(otgpg_config[CONF_PIN])
        cg.add(var.register_isl_component(isl9238))
        cg.add(var.register_binaryGPIO_sensor(CONF_SENS_OTGPG, otgpg, gpio))

    cg.add(isl9238.register_sensors(var))

#     if CONF_DEBUG in config:
#         #Trickle Charging mode
#         #Low_VSYS_Prochot# is tripped
#         #DCProchot# is tripped
#         #ACProchot#/OTGCURRENTProchot# is tripped
#         #control loop
#         #internal reference active

#         #Program Resister
#             # # of battery cells
#             #Switching Frequency
#             #Adapter current Limit
#         #operation mode
#         #state machine status
#         #BATGONE pin status
#         #general purpose comparator
#         #ACOK
#         pass