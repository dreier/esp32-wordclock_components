// #include "isl9238.h"
// #include "esphome/core/log.h"


// #define ISL9238_CONST_MAX_ChargeCurrent 6080//mA
// #define ISL9238_CONST_MAX_AdapterCurrent 6080//mA
// #define ISL9238_CONST_MAX_ACProchot 6400//mA
// #define ISL9238_CONST_MAX_DCProchot 12800//mA
// #define ISL9238_CONST_MAX_MaxSystemVoltage 18304//mV
// #define ISL9238_CONST_MAX_MinSystemVoltage 13824//mV
// #define ISL9238_CONST_MAX_InputVoltageLimit 18432//mV

// #define ISL9238_BIT_AutonomousChargingMode 7
// #define ISL9238_BIT_BatteryLearnMode 12
// #define ISL9238_BIT_TurboMode 6
// #define ISL9238_BIT_BatteryShipMode 10
// #define ISL9238_BIT_SMBusTimeout 7
// #define ISL9238_BIT_WOCP 1
// #define ISL9238_BIT_BatteryOVP 0

// #define ISL9238_DEFAULT_AdapterCurrent 1024//mA
// #define ISL9238_DEFAULT_ChargeCurrent 512//mA
// #define ISL9238_DEFAULT_MinSysVoltage (3300*2)//mV 
// #define ISL9238_DEFAULT_MaxSysVoltage (4200*2)//mV
// #define ISL9238_DEFAULT_ACProchot 6400//mA
// #define ISL9238_DEFAULT_DCProchot 8192//mA
// #define ISL9238_DEFAULT_InputVoltageLimit 18432//mv

// namespace esphome{
//     namespace isl9238{
//         static const char *const TAG = "isl9238";

//         //https://www.farnell.com/datasheets/2712666.pdf

//         const uint8_t ISL9238_REGISTER_CHARGECURRENTLIMIT = 0x14;
//         const uint8_t ISL9238_REGISTER_MAXSYSTEMVOLTAGE = 0x15;

//         const uint8_t ISL9238_REGISTER_T1ANDT2 = 0x38;
//         const uint8_t ISL9238_REGISTER_CONTROL0 = 0x39;
//         const uint8_t ISL9238_REGISTER_INFORMATION1 = 0x3A;
//         const uint8_t ISL9238_REGISTER_ADAPTERCURRENTLIMIT2 = 0x3B;
//         const uint8_t ISL9238_REGISTER_CONTROL1 = 0x3C;
//         const uint8_t ISL9238_REGISTER_CONTROL2 = 0x3D;
//         const uint8_t ISL9238_REGISTER_MINSYSTEMVOLTAGE = 0x3E;
//         const uint8_t ISL9238_REGISTER_ADAPTERCURRENTLIMIT1 = 0x3F;

//         const uint8_t ISL9238_REGISTER_REVISION_ID = 0x44;

//         const uint8_t ISL9238_REGISTER_ACPROCHOT = 0x47;
//         const uint8_t ISL9238_REGISTER_DCPROCHOT = 0x48;
//         const uint8_t ISL9238_REGISTER_OTG_VOLTAGE = 0x49;
//         const uint8_t ISL9238_REGISTER_OTG_CURRENT = 0x4A;
//         const uint8_t ISL9238_REGISTER_VINVOLTAGE = 0x4B;
//         const uint8_t ISL9238_REGISTER_CONTROL3 = 0x4C;
//         const uint8_t ISL9238_REGISTER_INFORMATION2 = 0x4D;
//         const uint8_t ISL9238_REGISTER_CONTROL4 = 0x4E;

//         const uint8_t ISL9238_REGISTER_MANUFACTURER_ID = 0xFE;
//         const uint8_t ISL9238_REGISTER_DEVICE_ID = 0xFF;
        

//         void ISL9238Component::setup() {
//             ESP_LOGCONFIG(TAG, "Setting up ISL9238..."); 
//             uint16_t reg;
//             if ((this->read_register(ISL9238_REGISTER_DEVICE_ID, (uint8_t*)(&reg),2,true) != 0) || (reg != ((uint16_t)(0x000C)))) {
//                 this->mark_failed();
//                 return;
//             }
//             /*            
//             this->prochot_pin_ = prochot_pin_;
//             this->otgpg_pin_ = otgpg_pin_;
//             this->acok_pin_ = acok_pin_;
//             this->psys_pin_ = psys_pin_;
//             this->abmon_pin_ = abmon_pin_;
            
//             this->cs_pin_->pin_mode(gpio::FLAG_OUTPUT);
//             this->clk_pin_->pin_mode(gpio::FLAG_OUTPUT);
//             this->dio_pin_->pin_mode(gpio::FLAG_OUTPUT);
//             this->set_write_protect_(false);
//             this->start_(true); 
//             */

//             //setze Systemspannung, Strom, Zellenzahl und messe Stromverbrauch
//             setMinSysVoltage(ISL9238_DEFAULT_MinSysVoltage);
//             setMaxSysVoltage(ISL9238_DEFAULT_MaxSysVoltage);
//             setBatteryShipMode(0);//1=enable 0=disable
//             setSMBusTimeout(1); //0=enable 1=disable
//             setWOCP(0);//0=enable 1=disable
//             setBatteryOVP(1);//1=enable 0=disable
//             setACProchot(ISL9238_DEFAULT_ACProchot);
//             setDCProchot(ISL9238_DEFAULT_DCProchot);
//             setAdapterCurrentLimit(ISL9238_DEFAULT_AdapterCurrent);
//             setChargingCurrentLimit(ISL9238_DEFAULT_ChargeCurrent);
//             setInputVoltageLimit(ISL9238_DEFAULT_InputVoltageLimit);
//             //setTurboMode(0); //0=enable 1=disable
//             //setAutonomousChargingMode(0); //0=enable 1=disable

//         }

//          void ISL9238Component::update() {
//             float temp = 42.0;

//             uint16_t data1 = readISLRegister(ISL9238_REGISTER_INFORMATION1);
//             uint16_t data2 = readISLRegister(ISL9238_REGISTER_INFORMATION2);
//             //ESP_LOGD(TAG, "ISL UPDATE=%.1f", temp);
//             ESP_LOGD(TAG, "ISL Info1=0x%04x", data1);
//             ESP_LOGD(TAG, "ISL Info2=0x%04x", data2);

//             if (this->isl_sensor_sysVoltage_ != nullptr){
//                 this->isl_sensor_sysVoltage_->publish_state(temp);
//             }       
//         }

//         void ISL9238Component::dump_config() {
//             ESP_LOGCONFIG(TAG, "isl9238:");
//             LOG_I2C_DEVICE(this);
//             LOG_UPDATE_INTERVAL(this);
//             if (this->is_failed()) {
//                 ESP_LOGE(TAG, "Communication with isl9238 failed!");
//             }
//             //LOG_PIN("  Pin CS: ", this->cs_pin_);
//             //LOG_PIN("  Pin CLK: ", this->clk_pin_);
//             //LOG_PIN("  Pin DIO: ", this->dio_pin_);
//         }

//         float ISL9238Component::get_setup_priority() const{
//             return setup_priority::DATA;
//         }

//         uint16_t ISL9238Component::readISLRegister(uint8_t registerAddress){
//             uint16_t data;
//             this->read_register(registerAddress, (uint8_t*)&data,2);
//             return data;
//         }

//         void ISL9238Component::writeISLRegister(uint8_t registerAddress, uint16_t data){
//             this->write_register(registerAddress, (uint8_t*)&data,2,true);
//         }

//         void ISL9238Component::setBitInISLRegister(uint8_t registerAddress, uint8_t bit, uint8_t val){
//             uint16_t data = readISLRegister(registerAddress);
//             data=(data&(~(1<<bit)))|((val>0)<<bit);
//             writeISLRegister(registerAddress, data);
//         } 

//         float ISL9238Component::getSysVoltage(){
//             return 0.0f;
//         }

//         float ISL9238Component::getAMON(){
//             return 0.0f;
//         }

//         float ISL9238Component::getBMON(){
//             return 0.0f;
//         }

//         float ISL9238Component::getPSYS(){
//             return 0.0f;
//         }

//         void ISL9238Component::setChargingCurrentLimit(uint16_t cur){
//             if(cur<=ISL9238_CONST_MAX_ChargeCurrent){
//                 writeISLRegister(ISL9238_REGISTER_CHARGECURRENTLIMIT, cur);
//             }else{
                
//             }
//         }
            
//         void ISL9238Component::setAdapterCurrentLimit(uint16_t cur){
//             if(cur<=ISL9238_CONST_MAX_AdapterCurrent){
//                 writeISLRegister(ISL9238_REGISTER_ADAPTERCURRENTLIMIT1, cur);
//             }else{
                
//             }
//         }
            
//         void ISL9238Component::setACProchot(uint16_t cur){
//             if(cur<=ISL9238_CONST_MAX_ACProchot){
//                 writeISLRegister(ISL9238_REGISTER_ACPROCHOT, cur);
//             }else{
                
//             }
//         }
            
//         void ISL9238Component::setDCProchot(uint16_t cur){
//             if(cur<=ISL9238_CONST_MAX_DCProchot){
//                 writeISLRegister(ISL9238_REGISTER_DCPROCHOT, cur);
//             }else{
                
//             }
//         }
            
//         void ISL9238Component::setMinSysVoltage(uint16_t vol){
//             if(vol<=ISL9238_CONST_MAX_MinSystemVoltage){
//                 writeISLRegister(ISL9238_REGISTER_MINSYSTEMVOLTAGE, vol);
//             }else{
                
//             }
//         }
            
//         void ISL9238Component::setMaxSysVoltage(uint16_t vol){
//             if(vol<=ISL9238_CONST_MAX_MaxSystemVoltage){
//                 writeISLRegister(ISL9238_REGISTER_MAXSYSTEMVOLTAGE, vol);
//             }else{
                
//             }
//         }

//         void ISL9238Component::setInputVoltageLimit(uint16_t vol){
//             if(vol<=ISL9238_CONST_MAX_InputVoltageLimit){
//                 writeISLRegister(ISL9238_REGISTER_VINVOLTAGE, vol);
//             }else{
                
//             }
//         }
            
//         void ISL9238Component::setAutonomousChargingMode(bool en){
//             setBitInISLRegister(ISL9238_REGISTER_CONTROL3,ISL9238_BIT_AutonomousChargingMode,en);
//         }
            
//         void ISL9238Component::setBatteryLearnMode(bool en){
//             setBitInISLRegister(ISL9238_REGISTER_CONTROL1,ISL9238_BIT_BatteryLearnMode,en);
//         }
            
//         void ISL9238Component::setTurboMode(bool en){
//             setBitInISLRegister(ISL9238_REGISTER_CONTROL1,ISL9238_BIT_TurboMode,en);
//         }
            
//         void ISL9238Component::setBatteryShipMode(bool en){
//             setBitInISLRegister(ISL9238_REGISTER_CONTROL3,ISL9238_BIT_BatteryShipMode,en);
//         }
            
//         void ISL9238Component::setSMBusTimeout(bool en){
//             setBitInISLRegister(ISL9238_REGISTER_CONTROL0,ISL9238_BIT_SMBusTimeout,en);
//         }
            
//         void ISL9238Component::setWOCP(bool en){
//             setBitInISLRegister(ISL9238_REGISTER_CONTROL2,ISL9238_BIT_WOCP,en);
//         }
            
//         void ISL9238Component::setBatteryOVP(bool en){
//             setBitInISLRegister(ISL9238_REGISTER_CONTROL2,ISL9238_BIT_BatteryOVP,en);
//         }
            
//     }
// }