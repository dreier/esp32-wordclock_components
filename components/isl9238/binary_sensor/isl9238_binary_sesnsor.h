#pragma once

// https://www.farnell.com/datasheets/2712666.pdf

#include "../isl9238.h"
#include "esphome/components/binary_sensor/binary_sensor.h"

namespace esphome {
namespace isl9238 {

typedef struct {
  binary_sensor::BinarySensor *sensor;
  InternalGPIOPin *pin;
} sensorPointers_t;

class ISL9238BinarySensor : public ISL9238SensorListener, public Component, binary_sensor::BinarySensor {
 private:
  std::map<std::string, sensorPointers_t> binarySensors;
  ISL9238Component *islComponent;

 public:
  void setup() override;
  void dump_config() override;
  void update() override;
  // float get_setup_priority() const override;

  void register_binaryExt_sensor(std::string id, binary_sensor::BinarySensor *sensor);
  void register_binaryGPIO_sensor(std::string id, binary_sensor::BinarySensor *sensor, InternalGPIOPin *pin);
  void register_isl_component(ISL9238Component *component) { islComponent = component; };

 protected:
};

}  // namespace isl9238
}  // namespace esphome