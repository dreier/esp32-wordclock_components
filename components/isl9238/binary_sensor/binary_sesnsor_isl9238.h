// #pragma once

// // https://www.farnell.com/datasheets/2712666.pdf

// #include "esphome/core/component.h"
// #include "esphome/core/hal.h"
// #include "esphome/components/sensor/sensor.h"
// #include "esphome/components/binary_sensor/binary_sensor.h"
// #include "esphome/components/gpio/binary_sensor/gpio_binary_sensor.h"
// #include "esphome/components/i2c/i2c.h"

// namespace esphome
// {
//     namespace isl9238
//     {
//         class ISL9238Component  : public PollingComponent, public sensor::Sensor, public i2c::I2CDevice {
//         private:
//             void setBitInISLRegister(uint8_t registerAddress, uint8_t bit, uint8_t val);
//             uint16_t readISLRegister(uint8_t registerAddress);
//             void writeISLRegister(uint8_t registerAddress, uint16_t data);

//         public:
//             void setup() override;
//             void dump_config() override;
//             void update() override;
//             float get_setup_priority() const override;

//             void set_prochot_pin(InternalGPIOPin *pin) { prochot_pin_ = pin; }
//             void set_otgpg_pin(InternalGPIOPin *pin) { otgpg_pin_ = pin; }
//             void set_acok_pin(InternalGPIOPin *pin) { acok_pin_ = pin; }
//             void set_psys_pin(InternalGPIOPin *pin) { psys_pin_ = pin; }
//             void set_abmon_pin(InternalGPIOPin *pin) { abmon_pin_ = pin; }
//             void set_batgone_pin(InternalGPIOPin *pin) { batgone_pin_ = pin; }

//             // constructor
//             //ISL9238Component();
//             //~ISL9238Component();
                
//             void update_sensor_state(sensor::Sensor *isl_sensor) { isl_sensor_sysVoltage_ = isl_sensor; }
//             // void update_sensor_state(sensor::Sensor *isl_sensor) { isl_sensor_AMON_ = isl_sensor; }
//             // void update_sensor_state(sensor::Sensor *isl_sensor) { isl_sensor_BMON_ = isl_sensor; }
//             // void update_sensor_state(sensor::Sensor *isl_sensor) { isl_sensor_PSYS_ = isl_sensor; }

//             void register_batgone(binary_sensor::BinarySensor *isl_sensor) { isl_sensor_batgone_ = isl_sensor;}

//             float getSysVoltage();
//             float getAMON();
//             float getBMON();
//             float getPSYS();
//             void setChargingCurrentLimit(uint16_t cur);
//             void setAdapterCurrentLimit(uint16_t cur);              
//             void setACProchot(uint16_t cur);                
//             void setDCProchot(uint16_t cur);                
//             void setMinSysVoltage(uint16_t vol);                
//             void setMaxSysVoltage(uint16_t vol);
//             void setInputVoltageLimit(uint16_t vol);                
//             void setAutonomousChargingMode(bool en);                
//             void setBatteryLearnMode(bool en);                
//             void setTurboMode(bool en);                
//             void setBatteryShipMode(bool en);                
//             void setSMBusTimeout(bool en);                
//             void setWOCP(bool en);                
//             void setBatteryOVP(bool en);

//         protected:
//             sensor::Sensor *isl_sensor_sysVoltage_{nullptr};
//             sensor::Sensor *isl_sensor_AMON_{nullptr};
//             sensor::Sensor *isl_sensor_BMON_{nullptr};
//             sensor::Sensor *isl_sensor_PSYS_{nullptr};
//             binary_sensor::BinarySensor *isl_sensor_batgone_{nullptr};

//             InternalGPIOPin *prochot_pin_;
//             InternalGPIOPin *otgpg_pin_;
//             InternalGPIOPin *acok_pin_;
//             InternalGPIOPin *psys_pin_;
//             InternalGPIOPin *abmon_pin_;
//             InternalGPIOPin *batgone_pin_;
//         };
//     }
// }