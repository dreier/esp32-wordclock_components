# import esphome.codegen as cg
# import esphome.config_validation as cv
# from esphome import pins
# from esphome.components import (
#     i2c, sensor, binary_sensor, gpio, voltage_sampler, adc
# )
# from esphome.const import (
#     CONF_ID,
#     CONF_PIN,
#     CONF_CURRENT,
#     ICON_BRIEFCASE_DOWNLOAD,
#     STATE_CLASS_MEASUREMENT,
#     UNIT_AMPERE,
#     UNIT_VOLT,
#     UNIT_WATT,
# )
# import constant as const

# CODEOWNERS = ["@Sven3er"]

# DEPENDENCIES = ["i2c"] #adc?

# MULTI_CONF = True

# isl9238_ns = cg.esphome_ns.namespace("isl9238")
# ISL9238Component = isl9238_ns.class_(
#     "ISL9238Component", cg.PollingComponent, i2c.I2CDevice, #voltage_sampler.VoltageSampler, #binary_sensor.BinarySensor
# )

# isl_schema_current = sensor.sensor_schema(
#     unit_of_measurement=UNIT_AMPERE,
#     icon=ICON_BRIEFCASE_DOWNLOAD,
#     accuracy_decimals=2,
#     state_class=STATE_CLASS_MEASUREMENT,
# )

# isl_schema_voltage = sensor.sensor_schema(
#     unit_of_measurement=UNIT_VOLT,
#     icon=ICON_BRIEFCASE_DOWNLOAD,
#     accuracy_decimals=2,
#     state_class=STATE_CLASS_MEASUREMENT,
# )

# isl_schema_watt = sensor.sensor_schema(
#     unit_of_measurement=UNIT_WATT,
#     icon=ICON_BRIEFCASE_DOWNLOAD,
#     accuracy_decimals=2,
#     state_class=STATE_CLASS_MEASUREMENT,
# )


# isl_binary_sensor_optional_pin = binary_sensor.binary_sensor_schema(
#     #icon=ICON_BRIEFCASE_DOWNLOAD,
# ).extend(
#     {
#         cv.Optional(CONF_PIN): cv.All(pins.internal_gpio_input_pin_schema)
#     }
# )


# CONFIG_SCHEMA = (
#     cv.Schema(
#         {
#             cv.GenerateID(): cv.declare_id(ISL9238Component),
#             cv.Optional(CONF_SENS_AMON): isl_schema_current, ## irgendwie die adc komponente hierfür verwenden
#             cv.Optional(CONF_SENS_BMON): isl_schema_current, ## wenn nur amon/bmon einmal per i2c konfigurtieren, wenn amon und bmon per i2c umkonfigurieren und abwechselt messen (ausnahmen, wenn nur eine Sapnnungsqulle angeschlossen)
#             cv.Optional(CONF_SENS_ACOK): cv.All(pins.internal_gpio_input_pin_schema),
#             cv.Optional(CONF_SENS_BATGONE):  isl_binary_sensor_optional_pin,
#             cv.Optional(CONF_SENS_OTGPG): cv.All(pins.internal_gpio_input_pin_schema),
#             cv.Optional(CONF_SENS_PROCHOT): cv.All(pins.internal_gpio_input_pin_schema),
#             cv.Optional(CONF_SENS_PSYS): isl_schema_watt, ## irgendwie die adc komponente hierfür verwenden
#             cv.Required(CONF_BAT_CELLS): cv.positive_int,
#             cv.Required(CONF_BAT_CAPACITY): cv.positive_float,
#             cv.Required(CONF_BAT_CURRENT_CHARGE): cv.positive_float,
#             cv.Required(CONF_BAT_CURRENT_DISCHARGE): cv.positive_float,
#         }
#     )
#     .extend(cv.polling_component_schema("15s"))
#     .extend(i2c.i2c_device_schema(0x09))
# )


# async def to_code(config):
#     var = cg.new_Pvariable(config[CONF_ID])
#     await cg.register_component(var, config)
#     await i2c.register_i2c_device(var, config)

#     #print(config)
#     #cg.add(var.set_battery_cellcount(config[CONF_BAT_CELLS]))
#     #cg.add(var.set_battery_capacity(config[CONF_BAT_CAPACITY]))
#     #cg.add(var.set_battery_curent_charge(config[CONF_BAT_CURRENT_CHARGE]))
#     #cg.add(var.set_battery_curent_discharge(config[CONF_BAT_CURRENT_DISCHARGE]))

#     if CONF_SENS_AMON in config:
#         sens = await sensor.new_sensor(config[CONF_SENS_AMON])
#         cg.add(var.update_sensor_state(sens))
#         amon = await cg.gpio_pin_expression(config[CONF_SENS_AMON])
#         cg.add(var.set_sel_pin(amon))
    
#     if CONF_SENS_BMON in config:
#         sens = await sensor.new_sensor(config[CONF_SENS_AMON])
#         cg.add(var.update_sensor_state(sens))
#         amon = await cg.gpio_pin_expression(config[CONF_SENS_AMON])
#         cg.add(var.set_sel_pin(amon))


#     if CONF_SENS_ACOK in config:
#         acok = await cg.gpio_pin_expression(config[CONF_SENS_ACOK])
#         cg.add(var.set_sel_pin(acok))

#     if batgone_config := config.get(CONF_SENS_BATGONE):
#         batgone = await binary_sensor.new_binary_sensor(batgone_config)
#         if CONF_PIN in batgone_config:
#             #if GPIO used treat as GPIO sensor
#             gpio = await cg.gpio_pin_expression(batgone_config[CONF_PIN])
#             cg.add(var.register_batgone(batgone))
#             cg.add(var.set_batgone_pin(gpio))
#         else:
#             #if no GPIO used read status via I2C         
#             cg.add(var.register_batgone(batgone))


#     if CONF_SENS_OTGPG in config:
#         otgpg = await cg.gpio_pin_expression(config[CONF_SENS_OTGPG])
#         cg.add(var.set_sel_pin(otgpg))
#     if CONF_SENS_PROCHOT in config:
#         prochot = await cg.gpio_pin_expression(config[CONF_SENS_PROCHOT])
#         cg.add(var.set_sel_pin(prochot))
#     if CONF_SENS_PSYS in config:
#         psys = await cg.gpio_pin_expression(config[CONF_SENS_PSYS])
#         cg.add(var.set_sel_pin(psys))

#     if CONF_DEBUG in config:
#         #Trickle Charging mode
#         #Low_VSYS_Prochot# is tripped
#         #DCProchot# is tripped
#         #ACProchot#/OTGCURRENTProchot# is tripped
#         #control loop
#         #internal reference active

#         #Program Resister
#             # # of battery cells
#             #Switching Frequency
#             #Adapter current Limit
#         #operation mode
#         #state machine status
#         #BATGONE pin status
#         #general purpose comparator
#         #ACOK
#         pass