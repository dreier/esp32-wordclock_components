import esphome.codegen as cg
import esphome.config_validation as cv
from esphome.components import (
    i2c,
)
from esphome.const import (
    CONF_ID,
)

CONF_BAT_CELLS="BAT_CELLS"
CONF_BAT_CAPACITY="BAT_CAPACITY"
CONF_BAT_CURRENT_CHARGE="BAT_CURRENT_CHARGE"
CONF_BAT_CURRENT_DISCHARGE="BAT_CURRENT_DISCHARGE"
CONF_DEBUG="debug"

CODEOWNERS = ["@Sven3er"]

DEPENDENCIES = ["i2c"]

MULTI_CONF = True

isl9238_ns = cg.esphome_ns.namespace("isl9238")
ISL9238Component = isl9238_ns.class_(
    "ISL9238Component", cg.PollingComponent, i2c.I2CDevice, #voltage_sampler.VoltageSampler, #binary_sensor.BinarySensor
)

CONF_ISL9238_ID = "isl9238_id"

CONFIG_SCHEMA = (
    cv.Schema(
        {
            cv.GenerateID(): cv.declare_id(ISL9238Component),
            cv.Required(CONF_BAT_CELLS): cv.positive_int,
            cv.Required(CONF_BAT_CAPACITY): cv.positive_float,
            cv.Required(CONF_BAT_CURRENT_CHARGE): cv.positive_float,
            cv.Required(CONF_BAT_CURRENT_DISCHARGE): cv.positive_float,
        }
    )
    .extend(i2c.i2c_device_schema(0x09))
    .extend(cv.polling_component_schema("15s")) 
)

FINAL_VALIDATE_SCHEMA = i2c.final_validate_device_schema(
    "isl9238_i2c",
    min_frequency=10000,
    max_frequency=400000,
)

async def to_code(config):
    var = cg.new_Pvariable(config[CONF_ID])
    await cg.register_component(var, config)
    await i2c.register_i2c_device(var, config)