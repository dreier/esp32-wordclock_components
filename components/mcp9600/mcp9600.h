#pragma once

#include "esphome/core/component.h"
#include "esphome/components/sensor/sensor.h"
#include "esphome/components/i2c/i2c.h"

namespace esphome {
namespace mcp9600 {

enum MCP9600ThermocoupleType : uint8_t {
  MCP9600_THERMOCOUPLE_TYPE_K = 0b000,
  MCP9600_THERMOCOUPLE_TYPE_J = 0b001,
  MCP9600_THERMOCOUPLE_TYPE_T = 0b010,
  MCP9600_THERMOCOUPLE_TYPE_N = 0b011,
  MCP9600_THERMOCOUPLE_TYPE_S = 0b100,
  MCP9600_THERMOCOUPLE_TYPE_E = 0b101,
  MCP9600_THERMOCOUPLE_TYPE_B = 0b110,
  MCP9600_THERMOCOUPLE_TYPE_R = 0b111,
};

enum MCP9600AlertChannel : uint8_t {
  MCP9600_ALERT_CHANNEL_1 = 0,
  MCP9600_ALERT_CHANNEL_2 = 1,
  MCP9600_ALERT_CHANNEL_3 = 2,
  MCP9600_ALERT_CHANNEL_4 = 3,
};

enum MCP9600AlertMode : uint8_t {
  MCP9600_THERMOCOUPLE_ALERT_MODE_COMPERATOR = 0b0,
  MCP9600_THERMOCOUPLE_ALERT_MODE_Interrupt = 0b1,
};

enum MCP9600AlertPolarity : uint8_t {
  MCP9600_THERMOCOUPLE_ALERT_POLARITY_LOW = 0b0,
  MCP9600_THERMOCOUPLE_ALERT_POLARITY_HIGH = 0b1,
};

enum MCP9600AlertDirection : uint8_t {
  MCP9600_THERMOCOUPLE_ALERT_DIRECTION_RISING = 0b0,
  MCP9600_THERMOCOUPLE_ALERT_DIRECTION_FALLING = 0b1,
};

enum MCP9600AlertSource : uint8_t {
  MCP9600_THERMOCOUPLE_ALERT_SOURCE_HOT = 0b0,
  MCP9600_THERMOCOUPLE_ALERT_SOURCE_COLD = 0b1,
};

typedef struct {
    float alert_temperature_limit;
    uint8_t alert_temperature_hysteresis;
    bool alert_enable : 1;
    MCP9600AlertMode alert_mode : 1;
    MCP9600AlertPolarity alert_output_polarity : 1;
    MCP9600AlertDirection alert_temperature_direction : 1;
    MCP9600AlertSource alert_source : 1;
} MCP9600AlertConfig_t;

class MCP9600Component : public PollingComponent, public i2c::I2CDevice {
 public:
  void setup() override;
  void dump_config() override;
  void update() override;

  float get_setup_priority() const override { return setup_priority::DATA; }

  void set_hot_junction(sensor::Sensor *hot_junction) { this->hot_junction_sensor_ = hot_junction; }
  void set_cold_junction(sensor::Sensor *cold_junction) { this->cold_junction_sensor_ = cold_junction; }
  void set_status(sensor::Sensor *status) { this->status_sensor_ = status; }
  void set_thermocouple_type(MCP9600ThermocoupleType thermocouple_type) {
    this->thermocouple_type_ = thermocouple_type;
  };

  void set_alert_temperature_limit(MCP9600AlertChannel channel, float temperature_limit)             { this->alert_configurations[channel].alert_temperature_limit = temperature_limit; }
  void set_alert_temperature_hysteresis(MCP9600AlertChannel channel, uint8_t temperature_hysteresis) { this->alert_configurations[channel].alert_temperature_hysteresis = temperature_hysteresis; }
  void set_alert_enable(MCP9600AlertChannel channel, bool enable)                                    { this->alert_configurations[channel].alert_enable = enable; }
  void set_alert_control_mode(MCP9600AlertChannel channel, MCP9600AlertMode mode)                    { this->alert_configurations[channel].alert_mode = mode; }
  void set_alert_output_polarity(MCP9600AlertChannel channel, MCP9600AlertPolarity polarity)         { this->alert_configurations[channel].alert_output_polarity= polarity; }
  void set_alert_temperature_direction(MCP9600AlertChannel channel, MCP9600AlertDirection direction) { this->alert_configurations[channel].alert_temperature_direction = direction; }
  void set_alert_temperature_source(MCP9600AlertChannel channel, MCP9600AlertSource source)          { this->alert_configurations[channel].alert_source = source; }

 protected:
  uint8_t device_id_{0};
  uint8_t revision_id_{0};

  sensor::Sensor *hot_junction_sensor_{nullptr};
  sensor::Sensor *cold_junction_sensor_{nullptr};
  sensor::Sensor *status_sensor_{nullptr};

  MCP9600ThermocoupleType thermocouple_type_{MCP9600_THERMOCOUPLE_TYPE_K};

  MCP9600AlertConfig_t alert_configurations[4] = {
    {
      .alert_temperature_limit = 0.0f,
      .alert_temperature_hysteresis = 0,
      .alert_enable = false,
      .alert_mode = MCP9600_THERMOCOUPLE_ALERT_MODE_COMPERATOR,
      .alert_output_polarity = MCP9600_THERMOCOUPLE_ALERT_POLARITY_HIGH,
      .alert_temperature_direction = MCP9600_THERMOCOUPLE_ALERT_DIRECTION_RISING,
      .alert_source = MCP9600_THERMOCOUPLE_ALERT_SOURCE_HOT
    },
    {
      .alert_temperature_limit = 0.0f,
      .alert_temperature_hysteresis = 0,
      .alert_enable = false,
      .alert_mode = MCP9600_THERMOCOUPLE_ALERT_MODE_COMPERATOR,
      .alert_output_polarity = MCP9600_THERMOCOUPLE_ALERT_POLARITY_HIGH,
      .alert_temperature_direction = MCP9600_THERMOCOUPLE_ALERT_DIRECTION_RISING,
      .alert_source = MCP9600_THERMOCOUPLE_ALERT_SOURCE_HOT
    },
    {
      .alert_temperature_limit = 0.0f,
      .alert_temperature_hysteresis = 0,
      .alert_enable = false,
      .alert_mode = MCP9600_THERMOCOUPLE_ALERT_MODE_COMPERATOR,
      .alert_output_polarity = MCP9600_THERMOCOUPLE_ALERT_POLARITY_HIGH,
      .alert_temperature_direction = MCP9600_THERMOCOUPLE_ALERT_DIRECTION_RISING,
      .alert_source = MCP9600_THERMOCOUPLE_ALERT_SOURCE_HOT
    },
    {
      .alert_temperature_limit = 0.0f,
      .alert_temperature_hysteresis = 0,
      .alert_enable = false,
      .alert_mode = MCP9600_THERMOCOUPLE_ALERT_MODE_COMPERATOR,
      .alert_output_polarity = MCP9600_THERMOCOUPLE_ALERT_POLARITY_HIGH,
      .alert_temperature_direction = MCP9600_THERMOCOUPLE_ALERT_DIRECTION_RISING,
      .alert_source = MCP9600_THERMOCOUPLE_ALERT_SOURCE_HOT
    }
};

  enum ErrorCode {
    NONE,
    COMMUNICATION_FAILED,
    FAILED_TO_UPDATE_CONFIGURATION,
  } error_code_{NONE};
};

}  // namespace mcp9600
}  // namespace esphome
