import esphome.codegen as cg
from esphome.components import i2c, sensor
import esphome.config_validation as cv
from esphome.const import (
    CONF_ID,
    CONF_THERMOCOUPLE_TYPE,
    DEVICE_CLASS_TEMPERATURE,
    STATE_CLASS_MEASUREMENT,
    UNIT_CELSIUS,
    STATE_CLASS_NONE,
    DEVICE_CLASS_EMPTY,
    UNIT_EMPTY
)

CONF_HOT_JUNCTION = "hot_junction"
CONF_COLD_JUNCTION = "cold_junction"
CONF_STATUS = "status"

CONF_ALERT1 = "temperature_alert1"
CONF_ALERT2 = "temperature_alert2"
CONF_ALERT3 = "temperature_alert3"
CONF_ALERT4 = "temperature_alert4"

CONF_ALERT_LIMIT = "limit"
CONF_ALERT_HYSTERESIS = "hysteresis"
CONF_ALERT_ENABLED = "enabled"
CONF_ALERT_MODE = "mode"
CONF_ALERT_POLARITY = "inverted"
CONF_ALERT_TEMPERATURE_DIRECTION = "temperature_direction"
CONF_ALERT_SOURCE = "source"

DEPENDENCIES = ["i2c"]
CODEOWNERS = ["@mreditor97"]

mcp9600_ns = cg.esphome_ns.namespace("mcp9600")
MCP9600Component = mcp9600_ns.class_(
    "MCP9600Component", cg.PollingComponent, i2c.I2CDevice
)

MCP9600ThermocoupleType = mcp9600_ns.enum("MCP9600ThermocoupleType")
THERMOCOUPLE_TYPE = {
    "K": MCP9600ThermocoupleType.MCP9600_THERMOCOUPLE_TYPE_K,
    "J": MCP9600ThermocoupleType.MCP9600_THERMOCOUPLE_TYPE_J,
    "T": MCP9600ThermocoupleType.MCP9600_THERMOCOUPLE_TYPE_T,
    "N": MCP9600ThermocoupleType.MCP9600_THERMOCOUPLE_TYPE_N,
    "S": MCP9600ThermocoupleType.MCP9600_THERMOCOUPLE_TYPE_S,
    "E": MCP9600ThermocoupleType.MCP9600_THERMOCOUPLE_TYPE_E,
    "B": MCP9600ThermocoupleType.MCP9600_THERMOCOUPLE_TYPE_B,
    "R": MCP9600ThermocoupleType.MCP9600_THERMOCOUPLE_TYPE_R,
}

MCP9600AlertChannel = mcp9600_ns.enum("MCP9600AlertChannel")

MCP9600ThermocoupleAlertModeType = mcp9600_ns.enum("MCP9600ThermocoupleAlertModeType")
THERMOCOUPLE_ALERT_MODE_ENUM= {
    "Comparator": MCP9600ThermocoupleAlertModeType.MCP9600_THERMOCOUPLE_ALERT_MODE_COMPERATOR,
    "Interrupt": MCP9600ThermocoupleAlertModeType.MCP9600_THERMOCOUPLE_ALERT_MODE_Interrupt,
}

MCP9600ThermocoupleAlertPolarityType = mcp9600_ns.enum("MCP9600ThermocoupleAlertPolarityType")
THERMOCOUPLE_ALERT_POLARITY_ENUM= {
    "High": MCP9600ThermocoupleAlertPolarityType.MCP9600_THERMOCOUPLE_ALERT_POLARITY_HIGH,
    "Low": MCP9600ThermocoupleAlertPolarityType.MCP9600_THERMOCOUPLE_ALERT_POLARITY_LOW,
}

MCP9600ThermocoupleAlertDirectionType = mcp9600_ns.enum("MCP9600ThermocoupleAlertDirectionType")
THERMOCOUPLE_ALERT_DIRECTION_ENUM= {
    "Rising": MCP9600ThermocoupleAlertDirectionType.MCP9600_THERMOCOUPLE_ALERT_DIRECTION_RISING,
    "Falling": MCP9600ThermocoupleAlertDirectionType.MCP9600_THERMOCOUPLE_ALERT_DIRECTION_FALLING,
}

MCP9600ThermocoupleAlertSourceType = mcp9600_ns.enum("MCP9600ThermocoupleAlertSourceType")
THERMOCOUPLE_ALERT_SOURCE_ENUM= {
    "Hot": MCP9600ThermocoupleAlertSourceType.MCP9600_THERMOCOUPLE_ALERT_SOURCE_HOT,
    "Cold": MCP9600ThermocoupleAlertSourceType.MCP9600_THERMOCOUPLE_ALERT_SOURCE_COLD,
}

CONFIG_SCHEMA = (
    cv.Schema(
        {
            cv.GenerateID(): cv.declare_id(MCP9600Component),
            cv.Optional(CONF_THERMOCOUPLE_TYPE, default="K"): cv.enum(
                THERMOCOUPLE_TYPE, upper=True
            ),
            cv.Optional(CONF_HOT_JUNCTION): sensor.sensor_schema(
                unit_of_measurement=UNIT_CELSIUS,
                accuracy_decimals=1,
                device_class=DEVICE_CLASS_TEMPERATURE,
                state_class=STATE_CLASS_MEASUREMENT,
            ),
            cv.Optional(CONF_COLD_JUNCTION): sensor.sensor_schema(
                unit_of_measurement=UNIT_CELSIUS,
                accuracy_decimals=1,
                device_class=DEVICE_CLASS_TEMPERATURE,
                state_class=STATE_CLASS_MEASUREMENT,
            ),
            cv.Optional(CONF_STATUS): sensor.sensor_schema(
                unit_of_measurement=UNIT_EMPTY,
                accuracy_decimals=1,
                device_class=DEVICE_CLASS_EMPTY,
                state_class=STATE_CLASS_NONE,
            ),
            
            cv.Optional(CONF_ALERT1): cv.Schema(
                {                   
                    cv.Required(CONF_ALERT_LIMIT): cv.temperature,
                    cv.Required(CONF_ALERT_HYSTERESIS): cv.temperature,
                    cv.Optional(CONF_ALERT_ENABLED, default=True): cv.boolean,
                    cv.Optional(CONF_ALERT_MODE, default="Comparator"): cv.enum(
                        THERMOCOUPLE_ALERT_MODE_ENUM, upper=False
                    ),
                    cv.Optional(CONF_ALERT_POLARITY, default="High"): cv.enum(
                        THERMOCOUPLE_ALERT_POLARITY_ENUM, upper=False
                    ),
                    cv.Optional(CONF_ALERT_TEMPERATURE_DIRECTION, default="Rising"): cv.enum(
                        THERMOCOUPLE_ALERT_DIRECTION_ENUM, upper=False
                    ),
                    cv.Optional(CONF_ALERT_SOURCE, default="Hot"): cv.enum(
                        THERMOCOUPLE_ALERT_SOURCE_ENUM, upper=False
                    ),
                }
            ),
            
            cv.Optional(CONF_ALERT2): cv.Schema(
                {                   
                    cv.Required(CONF_ALERT_LIMIT): cv.temperature,
                    cv.Required(CONF_ALERT_HYSTERESIS): cv.temperature,
                    cv.Optional(CONF_ALERT_ENABLED, default=False): cv.boolean,
                    cv.Optional(CONF_ALERT_MODE, default="Comparator"): cv.enum(
                        THERMOCOUPLE_ALERT_MODE_ENUM, upper=False
                    ),
                    cv.Optional(CONF_ALERT_POLARITY, default="High"): cv.enum(
                        THERMOCOUPLE_ALERT_POLARITY_ENUM, upper=False
                    ),
                    cv.Optional(CONF_ALERT_TEMPERATURE_DIRECTION, default="Rising"): cv.enum(
                        THERMOCOUPLE_ALERT_DIRECTION_ENUM, upper=True
                    ),
                    cv.Optional(CONF_ALERT_SOURCE, default="Hot"): cv.enum(
                        THERMOCOUPLE_ALERT_SOURCE_ENUM, upper=False
                    ),
                }
            ),
            
            cv.Optional(CONF_ALERT3): cv.Schema(
                {                   
                    cv.Required(CONF_ALERT_LIMIT): cv.temperature,
                    cv.Required(CONF_ALERT_HYSTERESIS): cv.temperature,
                    cv.Optional(CONF_ALERT_ENABLED, default=True): cv.boolean,
                    cv.Optional(CONF_ALERT_MODE, default="Comparator"): cv.enum(
                        THERMOCOUPLE_ALERT_MODE_ENUM, upper=False
                    ),
                    cv.Optional(CONF_ALERT_POLARITY, default="High"): cv.enum(
                        THERMOCOUPLE_ALERT_POLARITY_ENUM, upper=False
                    ),
                    cv.Optional(CONF_ALERT_TEMPERATURE_DIRECTION, default="Rising"): cv.enum(
                        THERMOCOUPLE_ALERT_DIRECTION_ENUM, upper=False
                    ),
                    cv.Optional(CONF_ALERT_SOURCE, default="Hot"): cv.enum(
                        THERMOCOUPLE_ALERT_SOURCE_ENUM, upper=False
                    ),
                }
            ),
            
            cv.Optional(CONF_ALERT4): cv.Schema(
                {                   
                    cv.Required(CONF_ALERT_LIMIT): cv.temperature,
                    cv.Required(CONF_ALERT_HYSTERESIS): cv.temperature,
                    cv.Optional(CONF_ALERT_ENABLED, default=True): cv.boolean,
                    cv.Optional(CONF_ALERT_MODE, default="Comparator"): cv.enum(
                        THERMOCOUPLE_ALERT_MODE_ENUM, upper=False
                    ),
                    cv.Optional(CONF_ALERT_POLARITY, default="High"): cv.enum(
                        THERMOCOUPLE_ALERT_POLARITY_ENUM, upper=False
                    ),
                    cv.Optional(CONF_ALERT_TEMPERATURE_DIRECTION, default="Rising"): cv.enum(
                        THERMOCOUPLE_ALERT_DIRECTION_ENUM, upper=False
                    ),
                    cv.Optional(CONF_ALERT_SOURCE, default="Hot"): cv.enum(
                        THERMOCOUPLE_ALERT_SOURCE_ENUM, upper=False
                    ),
                }
            ),
                        
        }
    )
    .extend(cv.polling_component_schema("60s"))
    .extend(i2c.i2c_device_schema(0x67))
)

FINAL_VALIDATE_SCHEMA = i2c.final_validate_device_schema(
    "mcp9600", min_frequency="10khz"
)


async def to_code(config):
    var = cg.new_Pvariable(config[CONF_ID])

    await cg.register_component(var, config)
    await i2c.register_i2c_device(var, config)

    cg.add(var.set_thermocouple_type(config[CONF_THERMOCOUPLE_TYPE]))

    if CONF_HOT_JUNCTION in config:
        conf = config[CONF_HOT_JUNCTION]
        sens = await sensor.new_sensor(conf)
        cg.add(var.set_hot_junction(sens))

    if CONF_COLD_JUNCTION in config:
        conf = config[CONF_COLD_JUNCTION]
        sens = await sensor.new_sensor(conf)
        cg.add(var.set_cold_junction(sens))
        
    if CONF_STATUS in config:
        conf = config[CONF_STATUS]
        sens = await sensor.new_sensor(conf)
        cg.add(var.set_status(sens))
        
    if CONF_ALERT1 in config:
        conf = config[CONF_ALERT1]
        cg.add(var.set_alert_temperature_limit      (MCP9600AlertChannel.MCP9600_ALERT_CHANNEL_1, conf[CONF_ALERT_LIMIT]))
        cg.add(var.set_alert_temperature_hysteresis (MCP9600AlertChannel.MCP9600_ALERT_CHANNEL_1, conf[CONF_ALERT_HYSTERESIS]))
        if CONF_ALERT_ENABLED  in conf:
            cg.add(var.set_alert_enable                 (MCP9600AlertChannel.MCP9600_ALERT_CHANNEL_1, conf[CONF_ALERT_ENABLED]))
        if CONF_ALERT_MODE  in conf:
            cg.add(var.set_alert_control_mode           (MCP9600AlertChannel.MCP9600_ALERT_CHANNEL_1, conf[CONF_ALERT_MODE]))
        if CONF_ALERT_POLARITY  in conf:
            cg.add(var.set_alert_output_polarity        (MCP9600AlertChannel.MCP9600_ALERT_CHANNEL_1, conf[CONF_ALERT_POLARITY]))
        if CONF_ALERT_TEMPERATURE_DIRECTION  in conf:
            cg.add(var.set_alert_temperature_direction  (MCP9600AlertChannel.MCP9600_ALERT_CHANNEL_1, conf[CONF_ALERT_TEMPERATURE_DIRECTION]))
        if CONF_ALERT_SOURCE  in conf:
            cg.add(var.set_alert_temperature_source     (MCP9600AlertChannel.MCP9600_ALERT_CHANNEL_1, conf[CONF_ALERT_SOURCE]))
        
    if CONF_ALERT2 in config:
        conf = config[CONF_ALERT2]
        cg.add(var.set_alert_temperature_limit      (MCP9600AlertChannel.MCP9600_ALERT_CHANNEL_2, conf[CONF_ALERT_LIMIT]))
        cg.add(var.set_alert_temperature_hysteresis (MCP9600AlertChannel.MCP9600_ALERT_CHANNEL_2, conf[CONF_ALERT_HYSTERESIS]))
        if CONF_ALERT_ENABLED  in conf:
            cg.add(var.set_alert_enable                 (MCP9600AlertChannel.MCP9600_ALERT_CHANNEL_2, conf[CONF_ALERT_ENABLED]))
        if CONF_ALERT_MODE  in conf:
            cg.add(var.set_alert_control_mode           (MCP9600AlertChannel.MCP9600_ALERT_CHANNEL_2, conf[CONF_ALERT_MODE]))
        if CONF_ALERT_POLARITY  in conf:
            cg.add(var.set_alert_output_polarity        (MCP9600AlertChannel.MCP9600_ALERT_CHANNEL_2, conf[CONF_ALERT_POLARITY]))
        if CONF_ALERT_TEMPERATURE_DIRECTION  in conf:
            cg.add(var.set_alert_temperature_direction  (MCP9600AlertChannel.MCP9600_ALERT_CHANNEL_2, conf[CONF_ALERT_TEMPERATURE_DIRECTION]))
        if CONF_ALERT_SOURCE  in conf:
            cg.add(var.set_alert_temperature_source     (MCP9600AlertChannel.MCP9600_ALERT_CHANNEL_2, conf[CONF_ALERT_SOURCE]))

    if CONF_ALERT3 in config:
        conf = config[CONF_ALERT3]
        cg.add(var.set_alert_temperature_limit      (MCP9600AlertChannel.MCP9600_ALERT_CHANNEL_3, conf[CONF_ALERT_LIMIT]))
        cg.add(var.set_alert_temperature_hysteresis (MCP9600AlertChannel.MCP9600_ALERT_CHANNEL_3, conf[CONF_ALERT_HYSTERESIS]))
        if CONF_ALERT_ENABLED  in conf:
            cg.add(var.set_alert_enable                 (MCP9600AlertChannel.MCP9600_ALERT_CHANNEL_3, conf[CONF_ALERT_ENABLED]))
        if CONF_ALERT_MODE  in conf:
            cg.add(var.set_alert_control_mode           (MCP9600AlertChannel.MCP9600_ALERT_CHANNEL_3, conf[CONF_ALERT_MODE]))
        if CONF_ALERT_POLARITY  in conf:
            cg.add(var.set_alert_output_polarity        (MCP9600AlertChannel.MCP9600_ALERT_CHANNEL_3, conf[CONF_ALERT_POLARITY]))
        if CONF_ALERT_TEMPERATURE_DIRECTION  in conf:
            cg.add(var.set_alert_temperature_direction  (MCP9600AlertChannel.MCP9600_ALERT_CHANNEL_3, conf[CONF_ALERT_TEMPERATURE_DIRECTION]))
        if CONF_ALERT_SOURCE  in conf:
            cg.add(var.set_alert_temperature_source     (MCP9600AlertChannel.MCP9600_ALERT_CHANNEL_3, conf[CONF_ALERT_SOURCE]))

    if CONF_ALERT4 in config:
        conf = config[CONF_ALERT4]
        cg.add(var.set_alert_temperature_limit      (MCP9600AlertChannel.MCP9600_ALERT_CHANNEL_4, conf[CONF_ALERT_LIMIT]))
        cg.add(var.set_alert_temperature_hysteresis (MCP9600AlertChannel.MCP9600_ALERT_CHANNEL_4, conf[CONF_ALERT_HYSTERESIS]))
        if CONF_ALERT_ENABLED  in conf:
            cg.add(var.set_alert_enable                 (MCP9600AlertChannel.MCP9600_ALERT_CHANNEL_4, conf[CONF_ALERT_ENABLED]))
        if CONF_ALERT_MODE  in conf:
            cg.add(var.set_alert_control_mode           (MCP9600AlertChannel.MCP9600_ALERT_CHANNEL_4, conf[CONF_ALERT_MODE]))
        if CONF_ALERT_POLARITY  in conf:
            cg.add(var.set_alert_output_polarity        (MCP9600AlertChannel.MCP9600_ALERT_CHANNEL_4, conf[CONF_ALERT_POLARITY]))
        if CONF_ALERT_TEMPERATURE_DIRECTION  in conf:
            cg.add(var.set_alert_temperature_direction  (MCP9600AlertChannel.MCP9600_ALERT_CHANNEL_4, conf[CONF_ALERT_TEMPERATURE_DIRECTION]))
        if CONF_ALERT_SOURCE  in conf:
            cg.add(var.set_alert_temperature_source     (MCP9600AlertChannel.MCP9600_ALERT_CHANNEL_4, conf[CONF_ALERT_SOURCE]))


