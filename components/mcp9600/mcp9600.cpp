#include "mcp9600.h"
#include "esphome/core/log.h"

namespace esphome {
namespace mcp9600 {

static const char *const TAG = "mcp9600";

static const uint8_t MCP9600_REGISTER_HOT_JUNCTION = 0x00;
// static const uint8_t MCP9600_REGISTER_JUNCTION_DELTA = 0x01; // Unused, but kept for future reference
static const uint8_t MCP9600_REGISTER_COLD_JUNTION = 0x02;
// static const uint8_t MCP9600_REGISTER_RAW_DATA_ADC = 0x03; // Unused, but kept for future reference
static const uint8_t MCP9600_REGISTER_STATUS = 0x04;
static const uint8_t MCP9600_REGISTER_SENSOR_CONFIG = 0x05;
static const uint8_t MCP9600_REGISTER_CONFIG = 0x06;
static const uint8_t MCP9600_REGISTER_ALERT_CONFIG_BASE = 0x08;
static const uint8_t MCP9600_REGISTER_ALERT1_CONFIG = MCP9600_REGISTER_ALERT_CONFIG_BASE + 0x00;
static const uint8_t MCP9600_REGISTER_ALERT2_CONFIG = MCP9600_REGISTER_ALERT_CONFIG_BASE + 0x01;
static const uint8_t MCP9600_REGISTER_ALERT3_CONFIG = MCP9600_REGISTER_ALERT_CONFIG_BASE + 0x02;
static const uint8_t MCP9600_REGISTER_ALERT4_CONFIG = MCP9600_REGISTER_ALERT_CONFIG_BASE + 0x03;
static const uint8_t MCP9600_REGISTER_ALERT_HYSTERESIS_BASE = 0x0C;
static const uint8_t MCP9600_REGISTER_ALERT1_HYSTERESIS = MCP9600_REGISTER_ALERT_HYSTERESIS_BASE + 0x00;
static const uint8_t MCP9600_REGISTER_ALERT2_HYSTERESIS = MCP9600_REGISTER_ALERT_HYSTERESIS_BASE + 0x01;
static const uint8_t MCP9600_REGISTER_ALERT3_HYSTERESIS = MCP9600_REGISTER_ALERT_HYSTERESIS_BASE + 0x02;
static const uint8_t MCP9600_REGISTER_ALERT4_HYSTERESIS = MCP9600_REGISTER_ALERT_HYSTERESIS_BASE + 0x03;
static const uint8_t MCP9600_REGISTER_ALERT_LIMIT_BASE = 0x10;
static const uint8_t MCP9600_REGISTER_ALERT1_LIMIT = MCP9600_REGISTER_ALERT_LIMIT_BASE + 0x00;
static const uint8_t MCP9600_REGISTER_ALERT2_LIMIT = MCP9600_REGISTER_ALERT_LIMIT_BASE + 0x01;
static const uint8_t MCP9600_REGISTER_ALERT3_LIMIT = MCP9600_REGISTER_ALERT_LIMIT_BASE + 0x02;
static const uint8_t MCP9600_REGISTER_ALERT4_LIMIT = MCP9600_REGISTER_ALERT_LIMIT_BASE + 0x03;
static const uint8_t MCP9600_REGISTER_DEVICE_ID = 0x20;

void MCP9600Component::setup() {
  ESP_LOGCONFIG(TAG, "Setting up MCP9600...");

  uint16_t dev_id = 0;
  this->read_byte_16(MCP9600_REGISTER_DEVICE_ID, &dev_id);
  this->device_id_ = (uint8_t) (dev_id >> 8);
  this->revision_id_ = (uint8_t) (dev_id & 0x00FF);

  // Allows both MCP9600's and MCP9601's to be connected.
  if (this->device_id_ != (uint8_t) 0x40 && this->device_id_ != (uint8_t) 0x41) {
    this->error_code_ = COMMUNICATION_FAILED;
    this->mark_failed();
    return;
  }
  bool success = this->write_byte(MCP9600_REGISTER_STATUS, 0x00);
  success |= this->write_byte(MCP9600_REGISTER_SENSOR_CONFIG, uint8_t(0x00 | thermocouple_type_ << 4));
  success |= this->write_byte(MCP9600_REGISTER_CONFIG, 0x00);
  float temp1;
  uint16_t temp2;
  for(uint8_t channel=0;channel<4;channel++){
    temp1 = (this->alert_configurations[channel].alert_temperature_limit);
    // temp2 = (uint16_t((temp1>=0.0f ? temp1 : (-1.0f*temp1)) * (1 << 4)) | uint16_t((temp1>=0.0f ? 0 : 1) << 15));
    // success |= this->write_byte_16(MCP9600_REGISTER_ALERT_LIMIT_BASE + channel, temp2);

    //convert the temp limit from a float to actual bits in the register
    temp2 = static_cast<uint16_t>(abs(temp1) * 4);
    temp2 = temp2 << 2;
    int16_t signedTempLimit = static_cast<int16_t>(temp2);
    if (temp1 < 0)
      signedTempLimit *= -1; //if the original temp limit was negative we shifted away the sign bit, so reapply it if necessary
    success |= this->write_byte_16(MCP9600_REGISTER_ALERT_LIMIT_BASE + channel, signedTempLimit);

    success |= this->write_byte(MCP9600_REGISTER_ALERT_HYSTERESIS_BASE + channel, uint8_t(this->alert_configurations[channel].alert_temperature_hysteresis));
    success |= this->write_byte(MCP9600_REGISTER_ALERT_CONFIG_BASE + channel,  0x00 | 
      ((this->alert_configurations[channel].alert_enable & 0b1) << 0) |
      ((this->alert_configurations[channel].alert_mode & 0b1) << 1) |
      ((this->alert_configurations[channel].alert_output_polarity & 0b1) << 2) |
      ((this->alert_configurations[channel].alert_temperature_direction & 0b1) << 3) |
      ((this->alert_configurations[channel].alert_source & 0b1) << 4)
    );
  }
  if (!success) {
    this->error_code_ = FAILED_TO_UPDATE_CONFIGURATION;
    this->mark_failed();
    return;
  }
}

// ToDo add callback for temperature alerts

void MCP9600Component::dump_config() {
  ESP_LOGCONFIG(TAG, "MCP9600:");
  LOG_I2C_DEVICE(this);
  LOG_UPDATE_INTERVAL(this);

  ESP_LOGCONFIG(TAG, "  Device ID: 0x%x", this->device_id_);
  ESP_LOGCONFIG(TAG,"  Revision ID: 0x%02x", this->revision_id_);

  LOG_SENSOR("  ", "Hot Junction Temperature", this->hot_junction_sensor_);
  LOG_SENSOR("  ", "Cold Junction Temperature", this->cold_junction_sensor_);

  switch (this->error_code_) {
    case COMMUNICATION_FAILED:
      ESP_LOGE(TAG, "Connected device does not match a known MCP9600 or MCP901 sensor");
      break;
    case FAILED_TO_UPDATE_CONFIGURATION:
      ESP_LOGE(TAG, "Failed to update device configuration");
      break;
    case NONE:
    default:
      break;
  }
}

void MCP9600Component::update() {
  if (this->hot_junction_sensor_ != nullptr) {
    uint16_t raw_hot_junction_temperature;
    if (!this->read_byte_16(MCP9600_REGISTER_HOT_JUNCTION, &raw_hot_junction_temperature)) {
      this->status_set_warning();
      return;
    }
    float hot_junction_temperature = int16_t(raw_hot_junction_temperature) * 0.0625;
    this->hot_junction_sensor_->publish_state(hot_junction_temperature);
  }

  if (this->cold_junction_sensor_ != nullptr) {
    uint16_t raw_cold_junction_temperature;
    if (!this->read_byte_16(MCP9600_REGISTER_COLD_JUNTION, &raw_cold_junction_temperature)) {
      this->status_set_warning();
      return;
    }
    float cold_junction_temperature = int16_t(raw_cold_junction_temperature) * 0.0625;
    this->cold_junction_sensor_->publish_state(cold_junction_temperature);
  }

  if (this->status_sensor_ != nullptr) {
    uint8_t status;
    if (!this->read_byte(MCP9600_REGISTER_STATUS, &status)) {
      this->status_set_warning();
      return;
    }
    this->status_sensor_->publish_state(status);
  }

  this->status_clear_warning();
}

// void MCP9600Component::setAlertEnable(MCP9600AlertChannel channel, bool enable){
//   alert_configurations
//   uint8_t tmp = this->read_byte(MCP9600_REGISTER_ALERT_CONFIG_BASE + channel);
//   this->write_byte(MCP9600_REGISTER_ALERT_CONFIG_BASE + channel,  ((tmp)&(~(1<<0)))|(enable?0b1:0b0) );
// }
// void MCP9600Component::setAlertControlMode(MCP9600AlertChannel channel, MCP9600AlertMode mode){
//   uint8_t tmp = this->read_byte(MCP9600_REGISTER_ALERT_CONFIG_BASE + channel);
//   this->write_byte(MCP9600_REGISTER_ALERT_CONFIG_BASE + channel,  ((tmp)&(~(1<<1)))|(mode?0b1:0b0) );  
// }
// void MCP9600Component::setAlertOutputPolarity(MCP9600AlertChannel channel, MCP9600AlertPolarity ploarity){
//   uint8_t tmp = this->read_byte(MCP9600_REGISTER_ALERT_CONFIG_BASE + channel);
//   this->write_byte(MCP9600_REGISTER_ALERT_CONFIG_BASE + channel,  ((tmp)&(~(1<<2)))|(ploarity?0b1:0b0) );  
// }
// void MCP9600Component::setAlertTemperatureDirection(MCP9600AlertChannel channel, MCP9600AlertDirection direction){
//   uint8_t tmp = this->read_byte(MCP9600_REGISTER_ALERT_CONFIG_BASE + channel);
//   this->write_byte(MCP9600_REGISTER_ALERT_CONFIG_BASE + channel,  ((tmp)&(~(1<<3)))|(direction?0b1:0b0) );  
// }
// void MCP9600Component::setAlertTemperatureSource(MCP9600AlertChannel channel, MCP9600AlertSource source){
//   uint8_t tmp = this->read_byte(MCP9600_REGISTER_ALERT_CONFIG_BASE + channel);
//   this->write_byte(MCP9600_REGISTER_ALERT_CONFIG_BASE + channel,  ((tmp)&(~(1<<4)))|(source?0b1:0b0) );  
// }
// void MCP9600Component::setAlertTemperatureHyteresis(MCP9600AlertChannel channel, uint8_t hysteresis){
//   this->write_byte(MCP9600_REGISTER_ALERT_HYSTERESIS_BASE + channel, hysteresis);
// }
// bool MCP9600Component::setAlertTemperatureLimit(MCP9600AlertChannel channel, float limit){
//   //convert the temp limit from a float to actual bits in the register
//   uint16_t unsignedTempLimit = static_cast<uint16_t>(abs(limit) * 4);
//   unsignedTempLimit = unsignedTempLimit << 2;
//   int16_t signedTempLimit = static_cast<int16_t>(unsignedTempLimit);
//   if (limit < 0)
//     signedTempLimit *= -1; //if the original temp limit was negative we shifted away the sign bit, so reapply it if necessary
//   return this->write_byte_16(MCP9600_REGISTER_ALERT_LIMIT_BASE + channel, signedTempLimit);
// }


}  // namespace mcp9600
}  // namespace esphome
